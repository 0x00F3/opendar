name := """opendar"""
organization := "io.opendar"

version := "0.0.36-RELEASE"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .enablePlugins(JavaAppPackaging)

scalaVersion := "2.12.6"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test

libraryDependencies += "com.typesafe.play" %% "play-mailer" % "6.0.1"
libraryDependencies += "com.typesafe.play" %% "play-mailer-guice" % "6.0.1"
libraryDependencies += "com.github.t3hnar" %% "scala-bcrypt" % "3.1"
//libraryDependencies += "org.mindrot" %% "jbcrypt" % "0.4"
libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "2.5.0"
libraryDependencies += "io.netty" % "netty-all" % "4.1.33.Final"
// Adds additional packages into Twirl
//TwirlKeys.templateImports += "app.opendar.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "app.opendar.binders._"

packageName in Docker := "opendar"
dockerExposedPorts := Seq(9000)
dockerRepository := Some("eholley")
