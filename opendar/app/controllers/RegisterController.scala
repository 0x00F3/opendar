package controllers;

/* controller for registering for a new account */

import com.github.t3hnar.bcrypt._
import domain._
import domain.dto._
import domain.identity._
import java.net.URLDecoder._
import javax.inject._
import models._
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.libs.mailer._
import play.api.mvc._
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class RegisterController @Inject()(cc: ControllerComponents, config: Configuration, mailerClient: MailerClient)
    extends AbstractController(cc)
{
  val confirmForm = Form (
    mapping(
      "email" -> text,
      "token" -> text
    )(ConfirmEmailModel.apply)(ConfirmEmailModel.unapply)
  );

  val registerForm = Form(
    mapping(
      "email" -> text,
      "password" -> text
    )(LoginModel.apply)(LoginModel.unapply)
  );

  val userRepo = new UserRepository(config.get[String]("connectionString"));

  def confirm = Action.async
  {
    implicit request: Request[AnyContent] =>
    {
      confirmForm.bindFromRequest.fold(
        formWithErrors => Future { BadRequest(<h1>Form model not bound</h1>) },
        confirm =>
        userRepo.confirm(decode(confirm.email, "UTF-8"), decode(confirm.token, "UTF-8"))
          .map { option => option match {
            case true => Ok
            case false => BadRequest(<h1>rejected token</h1>)
          }})
    }
  }

  def submit = Action.async
  {
    implicit request: Request[AnyContent] =>
    {
      registerForm.bindFromRequest.fold(formWithErrors =>
        Future { BadRequest },
        registration =>
        {
          attemptRegistrationLogin(registration)
        })
      } // end request
  } // end method
    //     */

  /*----------------------helpers---------------------------------------*/
  def attemptRegistrationLogin(registration: LoginModel): Future[Result] = 
  {
    new IdentityMan(userRepo, mailerClient)
      .registerOrAuthenticate(registration.email, registration.password.bcrypt)
      .map { auth => auth match {
        case true =>
          Ok("User with this password exists. You have been logged in.")
            .withSession("username" -> registration.email)
        case false => Created("Success.")
      }
      }
  }
} // end class
