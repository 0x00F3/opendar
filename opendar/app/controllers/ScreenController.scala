package controllers;

import javax.inject._
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc._
import websupport._

@Singleton
class ScreenController @Inject()(
  cc: ControllerComponents,
  config: Configuration,
  authenticated: AuthenticatedAction) extends AbstractController(cc)
{
  def month = authenticated
  {
    Ok(<h1>hello from the month action</h1>)

  }

  def list = authenticated
  {
    Ok(<h1>hello from the month action</h1>)
  }
}
