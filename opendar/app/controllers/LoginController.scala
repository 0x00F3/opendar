package controllers;

/* controller for logging in or registering for a new account */
import com.github.t3hnar.bcrypt._
import domain._
import domain.identity._
import javax.inject._
import models._
import play.api._
import play.api.data._
import play.api.libs.mailer._
import play.api.data.Forms._
import play.api.mvc._
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global


@Singleton
class LoginController @Inject()(cc: ControllerComponents, config: Configuration, mailerClient: MailerClient)
    extends AbstractController(cc)
{
  val loginForm = Form(
    mapping(
      "email" -> text,
      "password" -> text
    )(LoginModel.apply)(LoginModel.unapply)
  );

  val userRepo = new UserRepository(config.get[String]("connectionString"));

  /*
  def submit = Action.async
  {
    implicit request: Request[AnyContent] =>
    {
      loginForm.bindFromRequest.fold(formWithErrors =>
        Future { BadRequest(views.html.login()) },
        login =>
        {
          new IdentityMan(userRepo, mailerClient)
            .authenticate(login.email, login.password.bcrypt)
            .map { auth => auth match {
              case true => Ok("Successful login")
                  .withSession("username" -> login.email)
              case false => Unauthorized("Failed login")
            }
            }
        })
    } // end request
  } // end method */

  def submit = Action.async
  {
    implicit request: Request[AnyContent] =>
    {
      loginForm.bindFromRequest.fold(formWithErrors =>
        Future { BadRequest(views.html.login()) },
        login =>
        {
          new IdentityMan(userRepo, mailerClient)
            .getUser(login.email, login.password)
            .map(option => option match
            {
              case Some(user) => Ok(user.clientSalt)
                  .withSession("userId" -> user._id.toHexString())
              case None => Unauthorized("Failed login")
            })
        })
    } // end request
  } // end method

} // end class
