package domain;

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings.Builder
import dto._
import org.bson.codecs.configuration._
import org.bson.codecs.configuration.CodecRegistries._
import org.mongodb.scala._
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.connection.NettyStreamFactoryFactory

class OpendarCollections(connectionString: String)
{
  def getUserCollection: MongoCollection[User] =
  {
    getDatabase getCollection "user"
  }

  private[this] def getDatabase: MongoDatabase =
  {
    val settings: MongoClientSettings = MongoClientSettings.builder()
      .applyConnectionString(new ConnectionString(connectionString))
      .streamFactoryFactory(NettyStreamFactoryFactory())
      .build;
    MongoClient(settings)
      .getDatabase("opendar")
      .withCodecRegistry(getRegistry)
  }

  private[this] def getRegistry: CodecRegistry =
  {
    fromRegistries(DEFAULT_CODEC_REGISTRY, fromProviders(classOf[User]))
  }
}
