package domain.dto;

import java.util.Date
import org.mongodb.scala.bson._

case class User
(
  val _id: ObjectId,
  val clientSalt: String,
  val email: String,
  val emailConfirmed: Boolean,
  val passwordHash: String,
  val emailToken: String,
  val tokenDate: Date,
  val created: Date,
  val updated: Date
)
{
  def authenticate(email: String, passwordHash: String): Boolean = 
  {
    this.email == email && this.passwordHash == passwordHash
  }
}
