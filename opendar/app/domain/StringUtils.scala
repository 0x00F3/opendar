package domain

import java.math.BigInteger
import java.security.SecureRandom

object StringUtils
{
  def lpad(original: String, length: Int, padder: Char): String =
  {
    if(original.length() < length) lpad(padder + original, length, padder) else original
  }

  /* Stolen from https://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string/41156#41156
   * Explanation given: Why choosing length*5. Let's assume the simple case 
   * of a random string of length 1, so one random character. To get a random 
   * character containing all digits 0-9 and characters a-z, we would need a 
   * random number between 0 and 35 to get one of each character. BigInteger 
   * provides a constructor to generate a random number, uniformly 
   * distributed over the range 0 to (2^numBits - 1). Unfortunately 35 is no 
   * number which can be received by 2^numBits - 1. So we have two options: 
   * Either go with 2^5-1=31 or 2^6-1=63. If we would choose 2^6 we would get 
   * a lot of "unnecesarry" / "longer" numbers. Therefore 2^5 is the better 
   * option, even if we loose 4 characters (w-z). To now generate a string of 
   * a certain length, we can simply use a 2^(length*numBits)-1 number. The 
   * last problem, if we want a string with a certain length, random could 
   * generate a small number, so the length is not met, so we have to pad the 
   * string to it's required length prepending zeros. */
  def secureRandom(length: Int): String =
  {
    lpad(new BigInteger(length * 5, new SecureRandom()) toString 32, length, '0').replace(" ", "")
  }
}
