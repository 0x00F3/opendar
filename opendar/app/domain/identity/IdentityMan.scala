package domain.identity

/* identity manager to sit between the MongoDB repositories and the 
 * and the controllers */
import com.github.t3hnar.bcrypt._
import domain._
import domain.dto._
import java.math.BigInteger
import java.security.SecureRandom
import java.util.Date
import org.mongodb.scala.bson._
import play.api.libs.mailer._
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Failure
import scala.util.Success

case class IdentityMan(repo: UserRepository, mailerClient: MailerClient)
{
  def authenticate(email: String, passwordHash: String): Future[Boolean] =
  {
    repo.getUser(email: String, passwordHash: String)
      .map(option => option match {
        case None => false
        case Some(user) => user.emailConfirmed || register(email, passwordHash)
      })
  }

  def getUser(email: String, password: String): Future[Option[User]] =
  {
    repo.getConfirmedUsersByEmail(email)
      .map(seq => seq.filter(u => validate(password, u.passwordHash)))
      .map(seq => seq.headOption)
  }

  /* returns true if a possible login is successful. Fails over to
   * registration. 
   */
  def registerOrAuthenticate(email: String, passwordHash: String): Future[Boolean] =
  {
    repo.getConfirmedUser(email)
      .map(option => option match {
        case None => register(email, passwordHash)
        case Some(u) => authenticateElseWarn(u, email, passwordHash)
      })
  }

  /*-------------------------helper methods--------------------------------*/
  private[this] def getRandomToken(): String =
  {
    StringUtils.secureRandom(32)
  }

  private[this] def getSalt(): String =
  {
    StringUtils.secureRandom(16)
  }

  /* here be spaghetti */
  private[this] def register(email: String, passwordHash: String): Boolean = 
  {
    val confirmationToken = getRandomToken
    val registering: User = User(_id = new ObjectId(),
      getSalt(),
      email,
      emailConfirmed = false,
      passwordHash,
      emailToken = confirmationToken,
      tokenDate = new Date,
      created = new Date,
      updated = new Date);
    repo insert registering
    val confirmation = Email(subject = "Welcome to Opendar",
      from = "eleanorh@disroot.org",
      to = email :: Nil,
      bodyHtml =
        Some(templates.html.confirm(email, confirmationToken).toString())
    );
    mailerClient send confirmation
    false
  }

  
  private[this] def authenticateElseWarn(user: User, email: String, passwordHash: String) = 
  {
    if (user.authenticate(email, passwordHash))
      true
    else
    {
          val confirmation = Email(subject = "Welcome to Opendar",
      from = "eleanor.holley@gmail.com",
      to = email :: Nil,
      bodyHtml =
        Some(templates.html.emailExists().toString())
    );
    mailerClient send confirmation
    false
    }
  }

  private[this] def validate(plain: String, hashed: String): Boolean = 
  {
    plain.isBcryptedSafe(hashed) match
    {
      case Success(result) => result
      case Failure(_) => false //TODO better error handling. email the user or rethrow?
    }
  }

}
