package domain;

import dto._
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar
import org.mongodb.scala._
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global

class UserRepository(connectionString: String)
{
  System.setProperty("org.mongodb.async.type", "netty");

  /* TODO this method doesn't yet update the bit in the data base. */
  def confirm(email: String, token: String): Future[Boolean] =
  {
    val c: GregorianCalendar = new GregorianCalendar();
    c.setTime(new Date());
    c.add(Calendar.DAY_OF_MONTH, -1);
    val yesterday: Date = c.getTime();
/*    getUserCollection()
      .find(Filters.and(Filters.eq("email", email), Filters.eq("emailToken", token), Filters.gt("tokenDate", yesterday)))
      .toFuture()
      .map { seq => if ( seq.isEmpty) false else true }; */

    val findFilters: Bson = Filters.and(Filters.eq("email", email), Filters.eq("emailToken", token), Filters.gt("tokenDate", yesterday));
    
    getUserCollection()
      .findOneAndUpdate(findFilters, set("emailConfirmed", true))
      .toFuture()
      .map { before => if (before == null) false else true } 
  }

  def getConfirmedUser(email: String): Future[Option[User]] = 
  {
    Future { None }
    /*
    getUserCollection()
      .find(Filters.and(Filters.eq("email", email), Filters.eq("emailConfirmed", true)))
      .sort(Sorts.descending("created"))
      .toFuture()
      .map { seq => if (seq.isEmpty) None else Some(seq.head) }
     */
  }

  def getConfirmedUsersByEmail(email: String): Future[Seq[User]] =
  {
    getUserCollection()
      .find(Filters.and(Filters.eq("email", email), Filters.eq("emailConfirmed", true)))
      .sort(Sorts.descending("created"))
      .toFuture()
  }

  def getUser(email: String, passwordHash: String): Future[Option[User]] =
  {
    getUserCollection()
      .find(Filters.and(Filters.eq("email", email), Filters.eq("passwordHash", passwordHash)))
      .sort(Sorts.descending("created"))
      .toFuture()
      .map { seq => if (seq.isEmpty) None else Some(seq.head) }
  }

  def insert(u: User) =
  {
    getUserCollection()
      .insertOne(u)
      .toFuture()
  }

  /*------------------------helpers------------------------------------------*/

  private[this] def getUserCollection(): MongoCollection[User] =
  {
    new OpendarCollections(connectionString) getUserCollection
  }
}
