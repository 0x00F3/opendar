class RuntimeCache
{
    readonly name : string;
    constructor (name : string)
    {
        this.name = name;
    }


    respond(event: FetchEvent): Promise<Response>
    {
        console.warn('resorting to RUNTIME cache...');
        return caches.open(this.name)
            .then(cache => cache.match(event.request)
                  .then(match => {
                      if (match)
                      {
                          console.log('match found in RUNTIME cache.');
                          event.waitUntil(this.update(cache, event.request));
                          return match;
                      }
                      else
                      {
                          console.log('no match found in RUNTIME cache.');
                          event.waitUntil(this.update(cache, event.request));
                          return (fetch(event.request));
                      }
                  })
                 );
    }

    private update(cache: Cache, req: Request): Promise<void>
    {
        console.log('updating cache...');
        return fetch(req).then(response => cache.put(req, response));
    }

}


class OpendarWorker
{
    /* should always match the docker image tag */
    static readonly VERSION = "0.0.36-RELEASE"
    
    static readonly STATIC_CACHE = 'opendar-static-cache-' + OpendarWorker.VERSION;
    static readonly RUNTIME_CACHE = 'opendar-runtime-cache';

    readonly runtimeCache = new RuntimeCache(OpendarWorker.RUNTIME_CACHE);

    onActivate(event: ExtendableEvent)
    {
        event.waitUntil(caches.keys().then(keys => keys.map (key => { 
            if (key !== OpendarWorker.STATIC_CACHE
                && key !== OpendarWorker.RUNTIME_CACHE)
            {
                console.log('DELETING CACHE: ' + key);
                caches.delete(key);
            }
        })));
                                                             
    }

    //save me 
    onFetch(event: FetchEvent): void
    {
        if (event.request.url.includes("opendar.io"))
        {   
            event.respondWith(this.respondWithCache(event));
        }
        else
        {
            event.respondWith(fetch(event.request));
        }
    }
    
    onInstall(event: ExtendableEvent): void
    {
        event.waitUntil(
            caches.open(OpendarWorker.STATIC_CACHE)
                .then(cache =>
                      cache.addAll([
                          '/',
                          '/assets/elm/index.js',
                          '/assets/fonts/DejaVuSansMono-webfont.woff',
                          '/assets/images/icon-192.png',
                          '/assets/javascripts/wpack/dist/main.js',
                          '/assets/manifest.json',
                          '/assets/stylesheets/form.css',
                          '/assets/stylesheets/main.css',
                          '/assets/stylesheets/util.css',
                          '/assets/stylesheets/responsivity/large-devices.css',
                          '/assets/stylesheets/responsivity/medium-devices.css',
                          '/assets/stylesheets/responsivity/small-devices.css'
                      ]))
        )
        
        event.waitUntil(
            caches.open(OpendarWorker.RUNTIME_CACHE)
                .then(cache =>
                      cache.addAll([]))
        );

    }

    /*--------------------------private helpers-------------------------*/
    private respondWithCache(event: FetchEvent)
    {
        return caches.open(OpendarWorker.STATIC_CACHE)
            .then(cache => cache.match(event.request)
                  .then(match => {
                      if (match)
                      {
                          event.waitUntil(
                              this.update(cache, event.request)
                          );
                          return match;
                      }
                      else
                      {
                          console.warn('requested uncached resource: ');
                          console.warn(event.request.url);
                          return this.runtimeCache.respond(event);
                      }
                  })
                 );
    }

    private update(cache: Cache, req: Request)                 
    {
        fetch(req)
            .then(response => {
                console.log("Updating static cache...");
                console.log(req.url);
                cache.put(req, response);
            });
    }
    
}

const worker = new OpendarWorker();
self.addEventListener('install', e => worker.onInstall(<ExtendableEvent>e));
self.addEventListener('fetch', e => worker.onFetch(<FetchEvent>e));
self.addEventListener('activate', e => worker.onActivate(<ExtendableEvent>e));
