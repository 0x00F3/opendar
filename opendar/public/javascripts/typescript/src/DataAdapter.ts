import * as blockstack from './node_modules/blockstack/lib/index.js';
import { Calendar } from './Calendar.js';
import { DateSquare } from './DateSquare.js';
import { DeleteRequest } from './DeleteRequest.js';
import { Draft } from './Draft.js';
import { ElmActions } from './ElmActions.js';
import { Event } from './Event.js';
import { EventRepository } from './EventRepository.js';
import { Group } from './Group.js';
import { GroupRepository } from './GroupRepository.js';
import { Todo } from './Todo.js';
import { TodoRepository } from './TodoRepository.js';

/* stands between ElmActions and the repositories.*/

export class DataAdapter
{
    actions : ElmActions;
    events : EventRepository = new EventRepository();
    groups : GroupRepository = new GroupRepository();
    todos : TodoRepository = new TodoRepository();

    constructor ( actions : ElmActions )
    {
        this.actions = actions;
    }

    advanceMonth(): void
    {
        Calendar.advance().then(cal => this.actions.updateCalendar( cal ) );
    }

    decreaseMonth(): void
    {
        Calendar.decrease().then(cal => this.actions.updateCalendar( cal ) );
    }

    deleteItem ( req : DeleteRequest ) : void
    {
        if ( req.itemType === "event" )
        {
            this.events.delete(req.id)
                .then ( () => Calendar.latest() )
                .then ( cal => this.actions.updateCalendar( cal ) )
                .then ( () => DateSquare.latest() )
                .then ( dsq => this.actions.updateDateSquare( dsq ) );
        }
    }

    deleteTodo ( id : string )
    {
        this.todos.delete(id)
            .then(() => this.todos.getAll())
            .then(todos => this.actions.updateTodos(todos))
    }

    initialize() : void
    {
        console.log ('initializing data adapter...');
        Calendar.latest().then( cal => this.actions.updateCalendar(cal) )
            .then( () => DateSquare.latest() )
            .then(dsq => this.actions.updateDateSquare(dsq))
            .then( () => this.groups.getAll() )
            .then( groups => {
                this.actions.updateGroups(groups)
            })
            .then( () => this.todos.getAll() )
            .then( todos => this.actions.updateTodos(todos) )
    }

    redirectToSignIn() : void
    {
        console.log("redirecting to Blockstack sign in...");
        blockstack.redirectToSignIn();
    }
        
    save ( draft : Draft ) : void
    {
        console.log ('draft from elm: ');
        console.log (JSON.stringify ( draft ) );
        console.log ('');
        console.log ('');
        
        let group = new Group ( draft );
        
        if ( draft.groupId === '' )
        {
            console.log ('group in from elm: ');
            console.log (JSON.stringify ( group ) );
            console.log ('');
            console.log ('');
            this.groups.create ( group )
                .then( () => this.groups.getAll() )
                .then( groups => {
                    console.log ( 'groups to elm: ' );
                    console.log ( JSON.stringify( groups ) );
                    console.log('');
                    console.log('');
                    this.actions.updateGroups ( groups )
                });
        }

        /* determine object type and save. */
        if ( draft.startDate === '' )
        {
            this.todos.put ( new Todo ( draft, group.id ) )
                .then( () => this.todos.getAll() )
                .then( todos => this.actions.updateTodos ( todos ) );
        }
        else
        {
            this.events.put ( new Event ( draft, group.id ) )
                .then ( () => Calendar.latest() )
                .then ( cal => this.actions.updateCalendar( cal ) )
                .then (() => DateSquare.latest() )
                .then (dsq => this.actions.updateDateSquare( dsq ) );
        }   
    }

    setDateSquare(millis: number): void
    {
        DateSquare.set(millis).then(dsq => this.actions.updateDateSquare(dsq));
    }
}
