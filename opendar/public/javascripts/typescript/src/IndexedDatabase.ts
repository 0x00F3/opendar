export class IndexedDatabase
{
    idb : Promise<IDBDatabase> =
        new Promise<IDBDatabase>((resolve, reject) => {
            const req = window.indexedDB.open('opendar', 1);
            req.onupgradeneeded = () =>
                {
                    req.result.createObjectStore('daily', { keyPath: 'id' });
                    req.result.createObjectStore('event', { keyPath: 'id' });
                    req.result.createObjectStore('group', { keyPath: 'id' });
                    req.result.createObjectStore('monthly-on-date',
                                                 { keyPath: 'id' });
                    req.result.createObjectStore('monthly-on-weekday',
                                                 { keyPath: 'id' });
                    req.result.createObjectStore('todo', { keyPath: 'id' });
                    req.result.createObjectStore('weekly', { keyPath: 'id'});
                }
            req.onsuccess = () =>
                {
                    resolve(req.result)
                }
        });

    private static instance: IndexedDatabase | null = null;

    private constructor() { }

    public static getInstance(): IndexedDatabase
    {
        if (IndexedDatabase.instance === null)
        {
            IndexedDatabase.instance = new IndexedDatabase();
        }
        return IndexedDatabase.instance;
    }
}
