import { IndexedDatabase } from './IndexedDatabase.js';
import { Todo } from './Todo.js';

export class TodoRepository
{
    readwrite() : Promise<IDBObjectStore>
    {
        return IndexedDatabase.getInstance().idb
            .then(db => db.transaction( ['todo'], 'readwrite') )
            .then(transaction => transaction.objectStore('todo'));
    }

    delete( id : string ) : Promise<void>
    {
        return this.readwrite().then(objStore => objStore.delete(id))
            .then(req => new Promise<void>((resolve, reject) => {
                req.onsuccess = event => { resolve() };
            }));
    }

    getAll () :  Promise<Array<Todo>>
    {
        return this.readwrite().then(objStore => objStore.getAll())
            .then(req => new Promise<Array<Todo>>((resolve, reject) => {
                req.onsuccess = event => { resolve(<Array<Todo>>req.result) };
            }));
    }

    put (todo : Todo) : Promise<void>
    {
        return this.readwrite()
            .then(objStore =>
                  new Promise<void>((resolve, reject) => {
                      objStore.put(todo).onsuccess = () => { resolve() }
                  }));
    }

    
    
}
