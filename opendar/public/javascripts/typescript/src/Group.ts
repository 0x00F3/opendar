import { Draft } from './Draft.js';
import { Random } from './Random.js';

export class Group
{
    readonly background : string = "";
    readonly color : string = ""
    readonly id : string = "";
    readonly name : string = "";
    readonly updated : number;

    constructor (draft : Draft)
    {
        this.background = draft.groupColor;
        this.color = Group.generateColor (this.background);
        this.id = draft.groupId === '' ? Random.generateId() : draft.groupId
        this.name = draft.groupName;
        this.updated = Date.now();
    }

    private static generateColor ( background : string )
    {
        const r = parseInt (background.substr(1, 2), 16);
        const g = parseInt (background.substr(3, 2), 16);
        const b = parseInt (background.substr(5, 2), 16);

        // https://en.wikipedia.org/wiki/YIQ
        const yiq = ( (r * 299) + (g * 587) + (b * 114) ) / 1000;
        return (yiq >= 128) ? '#000000' : '#ffffff';
    }
}
