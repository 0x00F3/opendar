import { ImDate } from './ImDate.js';
import { Occurrence } from './Occurrence.js';

export class DateSquare
{
    millis : number; //millisecond when day began.
    local : string; //long-form representation of the date in locale
    occurrences : Array<Occurrence>;

    private static instance : Promise<DateSquare> | null = null;

    private constructor ( local : string
                          , millis : number
                          , occurrences: Array<Occurrence>
                        )
    {
//        console.log("initializing DateSquare for date: " + local);
        this.occurrences = occurrences;
        this.local = local;
        this.millis = millis;
    }

    static fromDate(imDate : ImDate) : Promise<DateSquare>
    {
        const options = { weekday: 'long',
                          year: 'numeric',
                          month: 'long',
                          day: '2-digit'
                        };
        return Occurrence.fromDate(imDate)
            .then(os => new DateSquare(imDate.format(options),
                                       imDate.toDate().getTime(),
                                       os
                                      ));
    }

    private static getInstance(): Promise<DateSquare>
    {
        if ( DateSquare.instance === null )
        {
            DateSquare.instance = DateSquare.initialize();
        }
        return <Promise<DateSquare>>DateSquare.instance;
    }

    private static initialize() : Promise<DateSquare>
    {
        return DateSquare.fromDate(ImDate.now());
    }

    public static latest() : Promise<DateSquare>
    {
        DateSquare.instance = DateSquare.getInstance()
            .then(dsq => new ImDate(new Date(dsq.millis)))
            .then(imDate => DateSquare.fromDate(imDate));
        return DateSquare.getInstance();
    }

    public static set(millis: number): Promise<DateSquare>
    {
        DateSquare.instance = DateSquare.fromDate(new ImDate(new Date(millis)));
        return DateSquare.getInstance();
    }
}
