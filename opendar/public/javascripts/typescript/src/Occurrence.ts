import { Draft } from './Draft.js';
import { Event } from './Event.js';
import { EventRepository } from './EventRepository.js';
import { ImDate } from './ImDate.js';

export class Occurrence
{
    static readonly events : EventRepository = new EventRepository();
    
    readonly draft : Draft;
    readonly end : string;
    readonly start : string;
    readonly startMillis : number; //not currently in elm equivalent

    private constructor(draft: Draft, end: string, start: string, startMillis: number)
    {
        this.draft = draft;
        this.end = end;
        this.start = start;
        this.startMillis = startMillis;
    }

    static fromDate(imDate: ImDate): Promise<Array<Occurrence>>
    {
        const test = this.events.getAll()
            .then(es =>
                  es.filter(e => Event.match(e, imDate.toDate())))
            .then(t => {
                if (t.length > 0)
                    console.log('Occurrence: ' + JSON.stringify(t));
            });
        return this.events.getAll()
            .then(es =>
                  es.filter(e => Event.match(e, imDate.toDate())))
            .then(es => es.map(e => Occurrence.fromEvent(e)))
            .then(os => os.slice().sort((p, q) => p.startMillis - q.startMillis));
    }

    private static fromEvent(event : Event): Occurrence
    {
        let options: Intl.DateTimeFormatOptions;
        if (Event.occursWithinDay(event))
        {
            options = { hour: '2-digit',
                        minute: '2-digit'
                      };
            
        }
        else
        {
            options = { weekday: 'long',
                        year: 'numeric',
                        month: 'long',
                        day: '2-digit',
                        hour: '2-digit',
                        minute: '2-digit'
                      };
        }

        const end : string = event.endMillis === null
            ? ''
            : Intl.DateTimeFormat([],options).format(new Date(<number>event.endMillis));
        const start : string = Intl
            .DateTimeFormat([], options).format(new Date(event.startMillis))
        return new Occurrence(Event.toDraft(event), end, start, event.startMillis);
    }
}
