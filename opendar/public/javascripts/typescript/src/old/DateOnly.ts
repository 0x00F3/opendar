import { StringUtils } from './StringUtils.js';

/* wraps a JavaScript Date with some helpful utilities. Truncates time */

export class DateOnly
{
    private date: Date;
    private constructor(date: Date)
    {
        this.date = date;
    }

    toLocalString(): string
    {
        const options =
            { weekday: 'long',
              year: 'numeric',
              month: 'long',
              day: '2-digit'
            };

        return Intl.DateTimeFormat([], options).format(this.date)
    }

    toIsoString(): string
    {
        return StringUtils.lpad(this.date.getUTCFullYear().toString(), 4, '0') + '-'
            + StringUtils.lpad((this.date.getUTCMonth() + 1).toString(), 2, '0') + '-'
            + StringUtils.lpad(this.date.getUTCDate().toString(), 2, '0');
    }


    static fromDate(date: Date): DateOnly
    {
        return new DateOnly(date);
    }

    static fromSeconds(seconds: number): DateOnly
    {
        return new DateOnly(new Date(seconds));
    }
}
