import { ConfirmEmail } from './ConfirmEmail.js';
import { IAction } from './Action.js';
import { Index } from './Index.js';
import { Login } from './Login.js';

export class Routes
{
    static all:Array<IAction> =
        [ new Index(),
          new Login(),
          new ConfirmEmail()
        ]
}
