/* a UTC wrapper/utility class around JavaScript
 * Dates */
export class DateTime
{
    private readonly date: Date;

    constructor(date: Date)
    {
        this.date = date;
    }

    static fromISOString(iso: string): DateTime
    {
        return new DateTime(new Date(Date.parse(iso)));
    }

    /* takes the values of HTMLInputElements (string
     * representations in the local time zone) and 
     * returns a UTC DateTime. */
    static fromLocalStrings(date: string, time: string)
    {
        return DateTime.fromLocal(new Date(Date.parse(date + 'T' + time)));
    }

    private static fromLocal(local: Date)
    {
        return new DateTime(new Date
                            (local.getUTCFullYear(),
                             local.getUTCMonth(),
                             local.getUTCDate(),
                             local.getUTCMinutes(),
                             local.getUTCSeconds()
                            )
                           );
    }
}
