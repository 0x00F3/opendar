export class Logger
{
    verbose: boolean;
    constructor(verbose: boolean)
    {
        this.verbose = verbose;
    }

    log(message: string)
    {
        console.log(message);
    }

    warn(message: string)
    {
        console.log(message);
    }
}
