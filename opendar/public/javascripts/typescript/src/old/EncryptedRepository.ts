import { NoKeyError } from './NoKeyError.js';

export class EncryptedRepository
{
    private static key: Promise<CryptoKey> | null = null;

    static setKey(plainPassword: string, salt: string): void
    {
        console.log('setting key...');
        const utf8encoder: TextEncoder = new TextEncoder()
        
        const params: Pbkdf2Params =
            {
                name: 'PBKDF2',
                hash: 'SHA-512',
                // mildly surprised the below line works, if it does. Beware.
                salt: utf8encoder.encode(salt),
                iterations: 1000000 // one million
            };
        const password: Uint8Array = utf8encoder.encode(plainPassword);
        const derivedKeyAlgorithm: AesKeyGenParams =
            {
                name: 'AES-GCM',
                length: 256
            };
        
        const master: PromiseLike<CryptoKey> = crypto.subtle
            .importKey('raw', password, 'PBKDF2', false, ['deriveBits', 'deriveKey'])
            .then(keyMaterial => crypto.subtle.deriveKey(params, keyMaterial, derivedKeyAlgorithm, false, ['encrypt', 'decrypt']));

        EncryptedRepository.key =
            new Promise ((resolve) => Promise.resolve(master));

        EncryptedRepository.key.then(() => console.log('key set...'));

    }

    /* TODO okay so clearly a lot of work to do here --eholley 2019-02-09 */
    fetch(): void //return type will be an iterable of tasks. 
    {
//        if (EncryptedRepository.key === null) throw new NoKeyError();
        //get list of data
        //return iterable that cycles through by date
    }
}
