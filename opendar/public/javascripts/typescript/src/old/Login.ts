import { Action } from './Action.js';
import { EncryptedRepository } from './EncryptedRepository.js';
import { LoginModel } from "./LoginModel.js";
import { RequestBuilder } from "./RequestBuilder.js";
import { Views } from "./Views.js";

export class Login extends Action
{    
    view: HTMLElement = document.querySelector('#template-login')
        || <HTMLElement>(document.createElement('template'));
    
    action(routeParameters: Array<string>): void
    {
        this.logger.log('executing Login page...');
        this.contentDiv.innerHTML = this.view.innerHTML;
        this.onLoad();
        if (routeParameters[1] == 'confirmed')
        {
            this.messageTarget().innerHTML
                += this.confirmedMessage().innerHTML;
        }
        this.logger.log('Login page loaded.');

    }

    confirmedMessage(): HTMLElement
    {
        return <HTMLElement>document
            .querySelector('#template-confirmed-email');
    }

    email(): string
    {
        const emailInput: HTMLInputElement =
        <HTMLInputElement>(document.querySelector('input#input-email'))
            || <HTMLInputElement>(document.createElement('input'));
        return emailInput.value;
    }

    form(): HTMLFormElement
    {
        return <HTMLFormElement>(document.querySelector('#form-login'))
    }

    handleLoginResponse(response: Response): void
    {
        this.logger.log('handling login response...');
        if (response.ok)
        {
            this.logger.log('login response was okay.');
            response.text().then(text => {
                EncryptedRepository.setKey(this.password(), text)
            }).then(() => window.location.replace('/#!/'));
        }
        else
        {
            this.loginBtn().onclick = () => this.onLoginClick();
            this.loginBtn().value = 'Log in';
            this.messageTarget().innerText = 'Login failed. You may check '
                + 'your password and try again. ';
        }
    }

    
    loginBtn(): HTMLInputElement
    {
        return <HTMLInputElement>document.getElementById('btn-login');
    }

    match(routeParameters: Array<string>): boolean
    {
        return routeParameters[0] == 'login';
    }

    messageTarget(): HTMLElement
    {
        return <HTMLElement>document.querySelector('#message-target');
    }

    onLoad(): void
    {
        this.registerBtn().onclick = () => this.onRegisterClick();
        this.loginBtn().onclick = () => this.onLoginClick();
    }
    
    onLoginClick(): void
    {
        this.loginBtn().value = "⌛ Logging in...";
        this.loginBtn().onclick = () => { }
        const req: Request = new RequestBuilder(this.csrfToken())
            .withBody(new FormData(this.form()))
            .withCache("no-cache")
            .withMethod("POST")
            .withUrl("/login")
            .build();
        
        fetch(req)
            .then(response => this.handleLoginResponse(response))
            .catch(error =>
                   this.logger.log('error while fetching.' + error.message));
    }
    
    onRegisterClick(): void
    {
        this.registerBtn().value = "⌛ Registering...";
        this.registerBtn().onclick = () => { }
        const req: Request = new RequestBuilder(this.csrfToken())
            .withBody(new FormData(this.form()))
            .withCache("no-cache")
            .withMethod("POST")
            .withUrl("/register")
            .build();
        fetch(req)
            .then(response => response.text())
            .then(text => this.logger.log('fetch response: ' + text))
            .then(() => this.onRegistered())
            .catch(error =>
                   this.logger.log('error while fetching.' + error.message));
    }

    onRegistered(): void
    {
        this.registerBtn().value = '✔️  Registered';
        this.messageTarget().innerHTML = this.registrationMessage().innerHTML;
    }

    password(): string
    {
        const passwordInput: HTMLInputElement =
            <HTMLInputElement>(document.querySelector('input#input-password'))
            || <HTMLInputElement>(document.createElement('input'));
        return passwordInput.value;
    }

    registerBtn(): HTMLInputElement
    {
        return <HTMLInputElement>document.getElementById('btn-register');
    }

    registrationMessage(): HTMLElement
    {
        return <HTMLElement>document
            .querySelector('#template-registration-message')
    }
}
