export class Views
{
    private static instance: Views = new Views()
    private  constructor() { }

    static getInstance(): Views
    {
        return Views.instance;
    }
    
    landing(): Promise<Response>{
        return fetch("/screen/list");
    }

    login(): Promise<string> {
        return fetch("/login")
            .then(response => response.text())
    }
        

    /*------------------------------helpers--------------------------------*/
    
    
}
