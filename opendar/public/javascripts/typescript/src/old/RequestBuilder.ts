/* a fluid interface for Requests */

export class RequestBuilder
{
    private body: string | FormData = '';
    private cache: "default" | "no-store" | "reload" | "no-cache" | "force-cache" = 'default';
    private credentials: "omit" | "same-origin" | "include" = 'same-origin';
    private headers: Headers; // = new Headers();
    private integrity: string = '';
    private keepAlive: boolean = false;
    private method: string = '';
    private mode: "same-origin" | "navigate" | "no-cors" | "cors" = "same-origin";
    private redirect: "follow" | "error" | "manual" = "error";
    private referrer: string = '';
    private referrerPolicy: "no-referrer" | "no-referrer-when-downgrade" | "same-origin" | "origin-when-cross-origin" = "same-origin";
    private url: string = '';

    constructor(csrfToken: string)
    {
        this.headers = new Headers();
        this.headers.set('Csrf-Token', csrfToken)
    }

    withBody(body: string | FormData): RequestBuilder
    {
        this.body = body;
        return this;
    }
    
    withCache(cache: "default" | "no-store" | "reload" | "no-cache" | "force-cache"): RequestBuilder
    {
        this.cache = cache
        return this;
    }

    withContentType(contentType: string): RequestBuilder
    {
        this.headers.set('Content-Type', contentType);
        return this
    }

    withCredentials(credentials: "omit" | "same-origin" | "include"): RequestBuilder
    {
        this.credentials = credentials
        return this;
    }

    withMethod(method: string): RequestBuilder
    {
        this.method = method;
        return this;
    }

    withMode(mode: "same-origin" | "navigate" | "no-cors" | "cors"): RequestBuilder
    {
        this.mode = mode;
        return this;
    }

    withUrl(url: string): RequestBuilder
    {
        this.url = url;
        return this;
    }

    //*/
    build(): Request
    {
        let initializer: RequestInit = {
//            arrayBuffer: this.arrayBuffer,
//            blob: this.blob,
            body: this.body,
//            bodyUsed: this.bodyUsed,
            cache: this.cache,
//            clone: this.clone,
            credentials: this.credentials,
//            destination: this.destination,
//            formData: this.formData,
            headers: this.headers,
            integrity: this.integrity,
//            json: this.json,
            keepalive: this.keepAlive,
            method: this.method,
            mode: this.mode,
            redirect: this.redirect,
            referrer: this.referrer,
//            referrerPolicy: this.referrerPolicy,
//            signal: this.signal,
//            text: this.text,
//            type: this.type,
//            url: this.url
        };
        return new Request(this.url, initializer);
    }

    //*/
}
