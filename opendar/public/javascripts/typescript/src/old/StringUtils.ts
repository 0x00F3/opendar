export class StringUtils
{
    static lpad(original: string, length: Number, padder: string): string
    {
        return original.length >= length
            ? original
            : StringUtils.lpad(padder + original, length, padder)
    }

    /* returns the string, or if type is not a string, returns empty string */
    static castOrEmpty<T>(obj: T): string
    {
        if (typeof obj === 'string') return <string>obj;
        else return '';
    }
}
