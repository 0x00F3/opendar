import { Action } from './Action.js';
import { Logger } from './Logger.js';
import { RequestBuilder } from './RequestBuilder.js';

export class ConfirmEmail extends Action
{
    action(routeParameters: Array<string>): void
    {
        this.logger.log('executing ConfirmEmail...');
        this.contentDiv.innerHTML = this.waitMessage().innerHTML;
        const postData: FormData = new FormData();
        postData.append('email', routeParameters[1]);
        postData.append('token', routeParameters[2]);
        const req: Request = new RequestBuilder(this.csrfToken())
            .withBody(postData)
            .withCache('no-cache')
            .withMethod('POST')
            .withUrl('/register/confirm')
            .build();
        fetch(req)
            .then(response => this.handleConfirmationResponse(response))
    }

    failureMessage(): HTMLElement
    {
        return <HTMLElement>document
            .querySelector('#template-confirm-email-failure');
    }

    handleConfirmationResponse(response: Response)
    {
        if (response.ok)
        {
            window.location.replace('/#!/login/confirmed');
        }
        else
        {
            this.contentDiv.innerHTML = this.failureMessage().innerHTML;
        }
    }

    match(routeParameters: Array<string>):boolean
    {
        return routeParameters[0] == 'confirm';
    }

    waitMessage(): HTMLElement
    {
        return <HTMLElement>document
            .querySelector('#template-confirming-email');
    }
}
