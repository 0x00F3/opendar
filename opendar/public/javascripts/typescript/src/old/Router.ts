import { IAction } from './Action.js';
import { Index } from './Index.js';
import { Logger } from './Logger.js';
import { NoKeyError } from './NoKeyError.js';
import { Routes } from './Routes.js';

export class Router
{
    logger: Logger = new Logger(true);
    match(url: string)
    {
        this.logger.log('finding route for: ' + url);
        const routeParameters: Array<string> =
            url.slice(3).toLowerCase().split('/');
        this.logger.log('Route Parameters: ');
        this.logger.log(routeParameters.toString());
        //TODO instead of Index, should be 404 page. 
        const route: IAction = Routes.all
            .find(r => r.match(routeParameters)) || new Index();

        try
        {
            route.action(routeParameters);
        }
        catch (error)
        {
            if (error instanceof NoKeyError)
            {
                this.logger.log('handling NoKeyError...');
                window.location.replace('/#!/login');
            }
            else throw error;
        }
    }
}        
    
