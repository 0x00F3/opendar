import { ListItem } from './ListItem.js';
import { Todo } from './Todo.js';

export class ListItemFactory
{
    create(data: FormData): ListItem
    {
        return Todo.fromForm(data);
    }
}
