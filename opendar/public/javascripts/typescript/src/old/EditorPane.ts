import { Option } from './Option.js';
import { ReminderNode } from './ReminderNode.js';

export class EditorPane
{
    blankReminder(): Node
    {
        return new ReminderNode(this.onReminderInput).build();
    }

    closeEditorBtn(): Option<HTMLInputElement>
    {
        return new Option(document.querySelector('#btn-close-editor'));
    }

    editor(): Option<HTMLFormElement> {
        return new Option(document.querySelector('#form-editor'))
    };

    editorSubmitBtn(): Option<HTMLInputElement> {
        return new Option(document.querySelector('#btn-editor-submit'));
    }

    firstReminderClear(): Option<HTMLElement> {
        return new Option(document.querySelector('#clear-first-reminder'));
    }
  	
    firstReminderDate(): Option<HTMLInputElement> {
        return new Option(document.querySelector('#date-first-reminder'));
    }

    firstReminderTime(): Option<HTMLInputElement> {
        return new Option(document.querySelector('#time-first-reminder'));
    }

    onEditorSubmit = () => {
        console.log('item submitted.');
        this.editor().map(form => new FormData(form))
            .map(data => console.log(data));
    }

    onLoad = () => {
        this.firstReminderClear()
            .map(input => input.onclick = this.onReminderClear);
        this.firstReminderDate()
            .map(input => input.oninput = this.onReminderInput);
        this.firstReminderTime()
            .map(input => input.oninput = this.onReminderInput);
        this.closeEditorBtn()
            .map(input => input.onclick = this.onCloseEditorClick);
        this.editorSubmitBtn()
            .map(input => input.onclick = this.onEditorSubmit);
    }
  	
    onReminderClear = () =>
        {
            console.log('first reminder cleared');
            this.firstReminderDate().map(input => input.value = '');
            this.firstReminderTime().map(input => input.value = '');
        }
  	
    reminderDiv(): Option<HTMLElement>
    {
        return new Option(document.querySelector('#div-reminders'));
    }
  
    appendReminder(): void
    {
        this.reminderDiv().map(div => div.appendChild(this.blankReminder()));
    }

    onCloseEditorClick = () => {
        this.scrollContainer()
            .map(element => element.classList.add('focussed'));
        this.editor()
            .map(form => form.classList.add('hidden'));
        this.editor()
            .map(form => form.classList.remove('focussed'));
    }

    
    onReminderInput = () =>
        {
            console.log('reminder changed.');
            this.editor().map(form => new FormData(form))
                .map(data => data.getAll('reminder.date[]')
                     .concat(data.getAll('reminder.time[]')))
                .map(all => all.filter(s => s === ''))
                .map(invalids =>
                     {
                         if (invalids.length === 0) this.appendReminder()
                     })            
        }
    
    scrollContainer(): Option<HTMLElement>
    {
        return new Option<HTMLElement>(document
                                       .querySelector('#scroll-container'));
    }
}
