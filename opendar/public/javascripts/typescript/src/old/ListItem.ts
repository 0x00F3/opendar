//import { Reminder } from './Reminder.js';

export interface ListItem
{
    readonly description: string;
    readonly group: string;
    readonly location: string;
    readonly notes: string;
//    readonly reminders: Array<Reminder>;
}
