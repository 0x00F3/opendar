import { Router } from './Router.js';

console.log('executing startup...');

document.addEventListener('DOMContentLoaded', () =>
                          new Router().match(location.hash));

window.onhashchange = () => new Router().match(location.hash);

console.log('startup script executed.');
