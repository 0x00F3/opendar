import { Action } from './Action.js';
import { DateOnly } from './DateOnly.js';
import { EditorPane } from './EditorPane.js';
import { EncryptedRepository } from './EncryptedRepository.js';
import { Option } from './Option.js';
import { Logger } from './Logger.js';
import { ScrollPane } from './ScrollPane.js';

export class Index extends Action
{   
    view: Option<HTMLElement> =
        new Option(document.querySelector('#template-index'));
    scrollPane: ScrollPane = new ScrollPane();
    editorPane: EditorPane = new EditorPane();
    action(routeparameters: Array<string>): void
    {
        this.logger.log('executing Index...');
        this.contentDiv.innerHTML = this.view
            .map(v => v.innerHTML)
            .getOrElse('Problem detected. "template-index" not found.');
        //this.loadTodos(DateOnly.fromSeconds(Date.now()));
      	this.scrollPane.loadTodos(DateOnly.fromSeconds(Date.now()));
        this.editorPane.onLoad();
/*        this.firstReminderClear()
            .map(input => input.onclick = this.onReminderClear);
        this.firstReminderDate()
            .map(input => input.oninput = this.onReminderInput);
        this.firstReminderTime()
            .map(input => input.oninput = this.onReminderInput);
        this.closeEditorBtn()
            .map(input => input.onclick = this.onCloseEditorClick);
*/
        this.repo.fetch() // TODO do something with this.
        this.logger.log('Index executed.');
    }

    blankReminder(): Node
    {
        return document.createRange()
            .createContextualFragment('<div>TODO</div>')
            .cloneNode(true);
    }

    //this is nothing. I need to build it out manually, not with template strings
    blankReminderString(): string
    {
        return `<div>
    <input name='reminder.date[]' type='date' />
    <input name='reminder.time[]' time='time' />
`
    }

/*    closeEditorBtn(): Option<HTMLInputElement>
    {
        return new Option(document.querySelector('#btn-close-editor'));
    }
    */
    editor(): Option<HTMLFormElement>
    {
        return new Option(document.querySelector('#form-editor'))
    };

    firstReminderClear(): Option<HTMLElement> {
        return new Option(document.querySelector('#clear-first-reminder'));
    }


    firstReminderDate(): Option<HTMLInputElement> {
        return new Option(document.querySelector('#date-first-reminder'));
    }

    firstReminderTime(): Option<HTMLInputElement> {
        return new Option(document.querySelector('#time-first-reminder'));
    }

    
    getButtonByIso(iso: string): HTMLInputElement
    {
        return <HTMLInputElement>document.querySelector('#btn-' + iso)
    }

    /* this gets one element in the scrolling list view. consider revisiting 
     * name.*/
    getDateElement(date: DateOnly): string
    {
        return `<span class='time-wrapper'>
  <time class='date-label'>
    ${date.toLocalString()}
  </time>
  <input class='btn-create' id='btn-${date.toIsoString()}'type='button' value='+' />
</span>`;
    }
		/*
    loadTodos(today: DateOnly): void
    {        
        this.scrollContainer().innerHTML
            += this.getDateElement(today);
        this.getButtonByIso(today.toIsoString())
            .onclick = () => this.onCreateClick(today.toIsoString());
    }*/

    match(routeParameters: Array<string>): boolean
    {
        return routeParameters.length == 0;
    }

    onReminderClear = () =>
        {
            console.log('first reminder cleared');
            this.firstReminderDate().map(input => input.value = '');
            this.firstReminderTime().map(input => input.value = '');
        }
    
    reminderDiv(): Option<HTMLElement>
    {
        return new Option(document.querySelector('#div-reminders'));
    }

    scrollContainer(): HTMLElement
    {
        return document.querySelector('#scroll-container')
            ||<HTMLElement>(document.createElement('template'));
    }

    appendReminder(): void
    {
        this.reminderDiv().map(div => div.appendChild(this.blankReminder()));
    }

    onCloseEditorClick = () =>
    {
         this.editor()
            .map(form => form.classList.add('hidden'));
    }
    
    onCreateClick(iso: string): void
    {
        console.log('date '+ iso + ' clicked.');
        this.editor()
            .map(form => form.classList.remove('hidden'));
    }

    onReminderInput = () =>
        {
            console.log('reminder changed.');
            this.editor().map(form => new FormData(form))
                .map(data => data.getAll('reminder.date[]')
                     .concat(data.getAll('reminder.time[]')))
                .map(all => all.filter(s => s === ''))
                .map(invalids =>
                     {
                         if (invalids.length === 0) this.appendReminder()
                     })            
        }
}
