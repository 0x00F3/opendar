import { EncryptedRepository } from './EncryptedRepository.js';
import { Option } from './Option.js';
import { Logger } from './Logger.js';

export interface IAction
{
    action(routeParameters: Array<string>): void;
    match(routeParameters: Array<string>): boolean;
}

export abstract class Action implements IAction
{
    contentDiv: HTMLElement = document.querySelector('#content')
        || new HTMLElement();
    logger: Logger = new Logger(true);
    repo: EncryptedRepository = new EncryptedRepository();

    csrfToken(): string
    {
        const input: HTMLInputElement =
            <HTMLInputElement>document.querySelector("[name='csrfToken']")
        return input.value
    }

    optionSelector(selectors: string): Option<Element>
    {
        return new Option(document.querySelector(selectors));
    }
    
    abstract action(routeParameters: Array<string>): void;
    abstract match(routeParameters: Array<string>): boolean;

}
