import { DateOnly } from './DateOnly.js';
import { Option } from './Option.js';

export class ScrollPane
{
    editor(): Option<HTMLFormElement>
    {
        return new Option(document.querySelector('#form-editor'))
    }
    
    getButtonByIso(iso: string): HTMLInputElement
    {
        return <HTMLInputElement>document.querySelector('#btn-' + iso)
    }
  
    /* this gets one element in the scrolling list view. consider revisiting 
     * name.*/
    getDateElement(date: DateOnly): string
    {
        return `<span class='time-wrapper'>
  <time class='date-label'>
    ${date.toLocalString()}
  </time>
  <input class='btn btn-default pull-right' 
         id='btn-${date.toIsoString()}'type='button' value='➕' />
</span>`;
    }
  
    loadTodos(today: DateOnly): void
    {        
        this.scrollContainer()
            .map(element => element.innerHTML
                 += this.getDateElement(today));
        this.getButtonByIso(today.toIsoString())
            .onclick = () => this.onCreateClick(today.toIsoString());
    }
  
    onCreateClick(iso: string): void
    {
        console.log('date '+ iso + ' clicked.');
        this.scrollContainer()
            .map(element => element.classList.remove('focussed'));
        this.editor()
            .map(form => form.classList.remove('hidden'));
        this.editor()
            .map(form => form.classList.add('focussed'));
    }
  
    scrollContainer(): Option<HTMLElement>
    {
    	return new Option<HTMLElement>(document
                                       .querySelector('#scroll-container'));
    }
}
