export class NoKeyError extends Error
{
    constructor()
    {
        super('Opendar has no encryption key stored now. Please log in.');
        this.name = 'NoKeyError'
    }
}
