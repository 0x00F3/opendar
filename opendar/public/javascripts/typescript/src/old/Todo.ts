import { ListItem } from './ListItem.js';
//import { Reminder } from './Reminder.js';
import { StringUtils } from './StringUtils.js';

export class Todo implements ListItem
{
    readonly description: string;
    readonly group: string;
    readonly location: string;
    readonly notes: string;
//    readonly reminders: Array<Reminder>;
    
    constructor
    (
        description: string, 
        group: string, 
        location: string, 
        notes: string, 
//        reminders: Array<Reminder>
    )
    {
        this.description = description;
        this.group = group;
        this.location = location;
        this.notes = notes;
//        this.reminders = reminders;
    }

    static fromForm(data: FormData): Todo
    {
        let description: string =
            StringUtils.castOrEmpty(data.get('description'));
        let group: string = 'default';
        let location: string =
            StringUtils.castOrEmpty(data.get('location'));
        let notes: string =
            StringUtils.castOrEmpty(data.get('notes'));
        return new Todo(description, group, location, notes);
    }
}
