/* I know it makes no sense to go to the trouble to
 * write my own immutable data structure, but it's 
 * just inevitable that I'm going to try to do it
 * that way anyway. */
export class List<T>
{
    private readonly underlying: Array<T>;

    private constructor(underlying: Array<T>)
    {
        this.underlying = underlying;
    }

    static fromItem<T>(item: T): List<T>
    {
        return new List<T>([item]);
    }

    static empty<T>(): List<T>
    {
        return new List<T>(new Array<T>());
    }
}
        
