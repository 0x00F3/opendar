import { Option } from './Option.js';

/* a class for creating "reminder" div items.
 * TODO This should probably all be refactored from
 * nested divs to ul/li semantic markup. Later.
 */
export class ReminderNode
{
    /* generally speaking, oninput checks if there are any empty / invalid 
     * reminder inputs, and, if there are none, adds another one to the DOM. 
     * Because this involves a lot of DOM manipulation, I'm injecting it.*/
    private readonly oninput: () => void;
    
    constructor(oninput:() => void)
    {
        this.oninput = oninput;
    }
    
    build(): Node
    {
        return this.group();
    }

    private clear(): Node
    {
        const result: HTMLInputElement =
            <HTMLInputElement>document.createElement('input');
        result.classList.add('btn');
        result.classList.add('btn-default');
        result.setAttribute('value', '❌'); 
        result.setAttribute('type', 'button');
        result.onclick = this.remove;
        return result;
    }

    private date(): Node
    {
        const result: HTMLInputElement =
            <HTMLInputElement>document.createElement('input');
        result.classList.add('grow-1');
        result.classList.add('textbox');
        result.setAttribute('name', 'reminder.date[]');
        result.setAttribute('type', 'date');
        result.oninput = this.oninput;
        return result;
    }
    
    private group(): Node
    {
        //reminder: const != immutable
        const group = document.createElement('div');
        group.classList.add('full-width');
        group.classList.add('flex-row');
        group.appendChild(this.date());
        group.appendChild(this.time());
        group.appendChild(this.clear());
        return group;
    }

    private remove = (event: Event) =>
    {
        new Option(event)
            .flatmap(event => new Option(event.target))
            .map(target => <HTMLInputElement>target)
            .flatmap(target => new Option(target.parentNode))
            .map(parentElement => (<ChildNode><any>parentElement).remove())
//            .map(grandparent => <ParentNode>grandparent.removeChild());
    }        

    private time(): Node
    {
        const result: HTMLInputElement =
            <HTMLInputElement>document.createElement('input');
        result.classList.add('textbox');
        result.setAttribute('name', 'reminder.time[]');
        result.setAttribute('type', 'time');
        result.oninput = this.oninput;
        return result;
    }
}
