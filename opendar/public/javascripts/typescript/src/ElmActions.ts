import { Calendar } from './Calendar.js';
import { DataAdapter } from './DataAdapter.js';
import { DateSquare } from './DateSquare.js';
import { DeleteRequest } from './DeleteRequest.js';
import { Draft } from './Draft.js';
import { Event } from './Event.js';
import { Group } from './Group.js';
import { Todo } from './Todo.js';

/* I want this to be the only file that elm-interop.js imports */
export class ElmActions
{
    updateCalendar : ( calendar : Calendar ) => void = (calendar) => {};

    updateDateSquare : ( dateSquare : DateSquare) => void = (dateSquare) => {};
    
    updateGroups : ( groups : Array<Group> ) => void = (groups) => {};

    updateTodos : ( todos : Array<Todo> ) => void = (todos) => {};

    private adapter : DataAdapter = new DataAdapter( this );

    command(command: string): void
    {
        if (command === "advanceMonth")
            this.adapter.advanceMonth();
        else if (command === "decreaseMonth")
            this.adapter.decreaseMonth();
        else if (command === "redirectToSignIn")
            this.adapter.redirectToSignIn()
    }

    deleteItem ( request : DeleteRequest ) : void
    {
        this.adapter.deleteItem(request);
    }

    deleteTodo ( id : string )
    {
        this.adapter.deleteTodo ( id );
    }
    
    initialize()
    {
        this.adapter.initialize()
    }

    save ( draft : Draft ) : void
    {
        this.adapter.save ( draft )
    }

    setDateSquare ( millis : number ): void
    {
        this.adapter.setDateSquare(millis);
    }
}
