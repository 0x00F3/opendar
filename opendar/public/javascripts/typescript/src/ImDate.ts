/* immutable date class */

export class ImDate
{
    private readonly member: Date;
    constructor (date: Date)
    {
        this.member = date;
    }

    copy(): ImDate
    {
        return new ImDate(new Date(this.member.getTime()));
    }

    getDate(): number
    {
        return this.toDate().getDate();
    }

    getDay(): number
    {
        return this.toDate().getDay();
    }

    /* gets month **index**, starting at zero */
    getMonth() : number
    {
        return this.toDate().getMonth();
    }

    getFullYear() : number
    {
        return this.toDate().getFullYear();
    }

    format(options : Intl.DateTimeFormatOptions) : string
    {
        return Intl.DateTimeFormat([], options).format(this.member);
    }

    next(): ImDate
    {
        return this.withDate(this.getDate() + 1);
    }

    previous(): ImDate
    {
        return this.withDate(this.getDate() - 1);
    }

    toDate(): Date
    {
        return this.member;
    }

    withDate(dayOfMonth: number): ImDate
    {
        const mutable = new Date(this.member.getTime());
        mutable.setDate(dayOfMonth);
        return new ImDate(new Date(mutable.getTime()));
    }

    /*------------------------------static----------------------------------*/

    public static now(): ImDate
    {
        return new ImDate(new Date(Date.now()))
    }

    public static firstDisplayDate(firstDay: number,
                                   month: number,
                                   year: number):ImDate
    {
        return ImDate
            .firstDisplayDateHelper(new ImDate(new Date(year, month, 1)), firstDay);
    }

    public static firstDisplayDateHelper(candidate: ImDate, firstDay: number):ImDate
    {
        return candidate.getDay() === firstDay
            ? candidate
            : ImDate.firstDisplayDateHelper(candidate.previous(), firstDay);
    }

        
}
