import { Group } from './Group.js';
import { IndexedDatabase } from './IndexedDatabase.js';
import { Option } from './Option.js';

export class GroupRepository
{
    readwrite() : Promise<IDBObjectStore>
    {
        return IndexedDatabase.getInstance().idb
            .then(db => db.transaction(['group'], "readwrite"))
            .then(transaction => transaction.objectStore('group'));
    }

    create (group : Group) : Promise<void>
    {
        return this.readwrite()
            .then(objStore => new Promise<void>((resolve, reject) => {
                      objStore.add(group).onsuccess = () => { resolve() }
            }));
    }

    getAll () : Promise<Array<Group>>
    {
        return this.readwrite().then(objStore => objStore.getAll())
            .then(req => new Promise<Array<Group>>((resolve, reject) => {
                req.onsuccess = event => { resolve(<Array<Group>>req.result) };
            }));
    }
    
}
