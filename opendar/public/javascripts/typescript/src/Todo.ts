import { Draft } from './Draft.js';
import { ElmActions } from './ElmActions.js';
import { Random } from './Random.js';
import { TodoRepository } from './TodoRepository.js';

export class Todo
{
    readonly description : string;
    readonly id : string;
    readonly group : string; //represents group id
    readonly location : string;
    readonly notes : string;
    readonly updated : number;

    constructor (draft : Draft, group : string)
    {
        this.description = draft.description;
        this.id = draft.id == ''
            ? Random.generateId()
            : draft.id;
        this.group = group;
        this.location = draft.location
        this.notes = draft.notes
        this.updated = Date.now();
    }
}
