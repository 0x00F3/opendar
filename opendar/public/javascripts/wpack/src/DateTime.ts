/* a UTC wrapper/utility class around JavaScript
 * Dates */
export class DateTime
{
    static toLocalDateString(date: Date): string
    {
        return date.getFullYear().toString()
            + '-'
            + (date.getMonth() + 1).toString().padStart(2, '0')
            + '-'
            + (date.getDate().toString()).padStart(2, '0');
    }

    static toLocalTimeString(date: Date): string
    {
        return date.getHours().toString().padStart(2, '0')
            + ":"
            + date.getMinutes().toString().padStart(2, '0');
    }
    
    /* takes the values of HTMLInputElements (string
     * representations in the local time zone) and 
     * returns a UTC DateTime. */
    static fromLocalStrings(date: string, time: string): Date
    {
        time = time === '' ? '' : 'T' + time + 'Z';
        console.log('fromLocalStrings: ');
        console.log(DateTime.fromLocal(new Date(Date.parse(date + time))));
        return DateTime.fromLocal(new Date(Date.parse(date + time)));
    }

    private static fromLocal(local: Date): Date
    {
        return new Date(new Date
                            (local.getUTCFullYear(),
                             local.getUTCMonth(),
                             local.getUTCDate(),
                             local.getUTCHours(),
                             local.getUTCMinutes(),
                             local.getUTCSeconds()
                            ).getTime()
                       );
    }

    static onSameDay(date1: Date, date2: Date): boolean
    {
        return date1.getFullYear() === date2.getFullYear()
            && date1.getMonth() === date2.getMonth()
            && date1.getDate() === date2.getDate();
    }
}
