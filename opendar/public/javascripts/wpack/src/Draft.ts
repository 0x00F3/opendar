
/* represents a generic Draft.elm draft sent from the "save" port. */
export class Draft
{
    readonly description : string = "";
    readonly endDate : string = "";
    readonly endTime : string = "";
    readonly groupColor : string = "";
    readonly groupId : string = ""
    readonly groupName : string = "";
    readonly id : string = "";
    readonly location : string = "";
    readonly notes : string = "";
    readonly startDate : string = "";
    readonly startTime : string = "";

}
