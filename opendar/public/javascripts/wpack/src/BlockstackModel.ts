/* a DTO corresponding to the Blockstack.elm model. */
export class BlockstackModel
{
    public readonly isSignedIn: boolean;
    public readonly username: string;

    private constructor(isSignedIn: boolean, username: string)
    {
        this.isSignedIn = isSignedIn;
        this.username = username;
    }

    public static fromUsername(username: string): BlockstackModel
    {
        return new BlockstackModel(true, username);
    }
    
    public static signedOut(): BlockstackModel
    {
        return new BlockstackModel(false, '');
    }
}
