import * as blockstack from 'blockstack';
import { Calendar } from './Calendar';
import { DateSquare } from './DateSquare';
import { DeleteRequest } from './DeleteRequest';
import { Draft } from './Draft';
import { ElmActions } from './ElmActions';
import { Event } from './models/Event';
import { Group } from './models/Group';
import { ILogger } from './core/Logger';
import { MasterRepository } from './data/MasterRepository';
import { Todo } from './models/Todo';

/* stands between ElmActions and the repositories.*/

export class DataAdapter
{
    readonly actions : ElmActions;
    readonly logger : ILogger;
    readonly master : MasterRepository;

    constructor ( actions : ElmActions, logger: ILogger )
    {
        this.actions = actions;
        this.logger = logger;
        this.master = new MasterRepository(logger);
    }

    authenticate(): void
    {
        this.master.remotes
            .authenticate(u => this.actions.handleUsername(u))
    }

    advanceMonth(): void
    {
        Calendar.advance(this.master)
            .then(cal => this.actions.updateCalendar( cal ) );
    }

    decreaseMonth(): void
    {
        Calendar.decrease(this.master)
            .then(cal => this.actions.updateCalendar( cal ) );
    }

    deleteItem ( req : DeleteRequest ) : void
    {
        if ( req.itemType === "event" )
        {
            this.master.events.delete(req.id)
                .then ( () => Calendar.latest(this.master) )
                .then ( cal => this.actions.updateCalendar( cal ) )
                .then ( () => DateSquare.latest(this.master) )
                .then ( dsq => this.actions.updateDateSquare( dsq ) );
        }
    }

    deleteTodo ( id : string )
    {
        this.master.todos.delete(id)
            .then(() => this.master.todos.getAll())
            .then(todos => this.actions.updateTodos(todos))
    }

    initialize() : void
    {
        this.logger.log ('initializing data adapter...');
        this.authenticate();
        Calendar.latest(this.master)
            .then( cal => this.actions.updateCalendar(cal) )
            .then( () => DateSquare.latest(this.master) )
            .then(dsq => this.actions.updateDateSquare(dsq))
            .then( () => this.master.groups.getAll() )
            .then( groups => {
                this.actions.updateGroups(groups)
            })
            .then( () => this.master.todos.getAll() )
            .then( todos => this.actions.updateTodos(todos) )
    }

    redirectToSignIn(): void
    {
        this.master.remotes.redirectToSignIn();
    }

    refreshRemote(): void
    {
        this.master.refreshRemote();
        
    }
        
    save ( draft : Draft ) : void
    {
        console.log ('draft from elm: ');
        console.log (JSON.stringify ( draft ) );
        console.log ('');
        console.log ('');
        
        let group = new Group ( draft );
        
        if ( draft.groupId === '' )
        {
            console.log ('group in from elm: ');
            console.log (JSON.stringify ( group ) );
            console.log ('');
            console.log ('');
            this.master.groups.create ( group )
                .then( () => this.master.groups.getAll() )
                .then( groups => {
                    console.log ( 'groups to elm: ' );
                    console.log ( JSON.stringify( groups ) );
                    console.log('');
                    console.log('');
                    this.actions.updateGroups ( groups )
                });
        }

        /* determine object type and save. */
        if ( draft.startDate === '' )
        {
            this.master.todos.put ( new Todo ( draft, group.id ) )
                .then( () => this.master.todos.getAll() )
                .then( todos => this.actions.updateTodos ( todos ) );
        }
        else
        {
            this.master.events.put ( new Event ( draft, group.id ) )
                .then ( () => Calendar.latest(this.master) )
                .then ( cal => this.actions.updateCalendar( cal ) )
                .then (() => DateSquare.latest(this.master) )
                .then (dsq => this.actions.updateDateSquare( dsq ) );
        }   
    }

    setDateSquare(millis: number): void
    {
        DateSquare.set(millis, this.master)
            .then(dsq => this.actions.updateDateSquare(dsq));
    }

    signOut(): void
    {
        this.master.remotes.signOut();
    }
}
