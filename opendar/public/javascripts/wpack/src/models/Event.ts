import { DateTime } from '../DateTime';
import { Draft } from '../Draft';
import { Random } from '../core/Random';

export class Event
{
    readonly description : string;
    readonly endMillis : number | null;
    readonly group : string; //represents group id
    readonly id : string;
    readonly location : string;
    readonly notes : string;
    readonly startMillis : number;
    readonly updated : number ;


    constructor (draft : Draft, group : string)
    {
        this.description = draft.description;
        this.endMillis = draft.endDate === ''
            ? null
            : DateTime
            .fromLocalStrings(draft.endDate, draft.endTime).getTime();
        this.id = draft.id == ''
            ? Random.generateId()
            : draft.id;
        this.group = group;
        this.location = draft.location;
        this.notes = draft.notes;
        this.startMillis =
            DateTime.fromLocalStrings(draft.startDate, draft.startTime)
            .getTime();
        this.updated = Date.now();
    }

    //returns true if any part of the event falls within the given day
    public static match(that: Event, date: Date): boolean
    {
        if (that.endMillis === null)
        {
            const start = new Date(that.startMillis);
            return start.getFullYear() === date.getFullYear()
                && start.getMonth() === date.getMonth()
                && start.getDate() === date.getDate();
        }
        else
        {
            //else end millis is not null so it's a number.
            const end = <number>that.endMillis;
            const nextDate = new Date(date.getFullYear(),
                                      date.getMonth(),
                                      date.getDate() + 1
                                     ).getTime();
            return (that.startMillis >= date.getTime()
                    && that.startMillis <= nextDate)
                || (end >= date.getTime() && end <= nextDate)
                || (that.startMillis <= date.getTime() && end >= nextDate);
        }
    }

    public static occursWithinDay(that: Event): boolean
    {
        return that.endMillis === null
            || DateTime.onSameDay(new Date(that.startMillis)
                                  , new Date(<number>that.endMillis));
    }
            
    public static toDraft(that: Event): Draft
    {
        return { description: that.description,
                 endDate: that.endMillis === null
                 ? '' : DateTime.toLocalDateString(new Date(that.endMillis)),
                 endTime: that.endMillis === null
                 ? '' : DateTime.toLocalTimeString(new Date(that.endMillis)),
                 groupColor: '', //n/a because the group isn't being edited.
                 groupId: that.group,
                 groupName: '', //n/a because the group isn't being edited.
                 id: that.id,
                 location : that.location,
                 notes: that.notes,
                 startDate: DateTime.toLocalDateString(new Date(that.startMillis)),
                 startTime: DateTime.toLocalTimeString(new Date(that.startMillis))
               };
    }
}
