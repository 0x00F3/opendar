import { BlockstackModel } from './BlockstackModel';
import { Calendar } from './Calendar';
import { DataAdapter } from './DataAdapter';
import { DateSquare } from './DateSquare';
import { DeleteRequest } from './DeleteRequest';
import { Draft } from './Draft';
import { Event } from './models/Event';
import { Group } from './models/Group';
import { ILogger, Logger } from './core/Logger';
import { Option } from './Option';
import { Todo } from './models/Todo';
//import { UserSession } from 'blockstack';

/* I want this to be the only file that elm-interop.js imports */
export class ElmActions
{
    debugLog : ( message : string ) => void = (message) => {};
    
    updateBlockstack : ( model : BlockstackModel ) => void = (model) => {};
    
    updateCalendar : ( calendar : Calendar ) => void = (calendar) => {};

    updateDateSquare : ( dateSquare : DateSquare) => void = (dateSquare) => {};
    
    updateGroups : ( groups : Array<Group> ) => void = (groups) => {};

    updateTodos : ( todos : Array<Todo> ) => void = (todos) => {};

    private logger : ILogger = new Logger(m => this.debugLog(m));
    private adapter : DataAdapter = new DataAdapter(this, this.logger);

    //private readonly masterAdapter: IMasterAdapter = new MasterAdapter();
/*    private readonly repo: MasterRepository =
        new MasterRepository(this.masterAdapter, this.logger); */

    //
    constructor()
    {
        
    }
        

    command(command: string): void
    {
        if (command === "advanceMonth")
            this.adapter.advanceMonth();
        else if (command === "decreaseMonth")
            this.adapter.decreaseMonth();
        else if (command === "redirectToSignIn")
            this.redirectToSignIn()
        else if (command === "signOut")
            this.signOut()
    }

    deleteItem ( request : DeleteRequest ) : void
    {
        this.adapter.deleteItem(request);
    }

    deleteTodo ( id : string )
    {
        this.adapter.deleteTodo ( id );
    }
    
    initialize()
    {
        this.adapter.initialize();
    }

    handleUsername(username: string): void
    {
        this.updateBlockstack(BlockstackModel.fromUsername(username));
        this.adapter.refreshRemote();
    }

    redirectToSignIn() : void
    {
        this.adapter.redirectToSignIn();
    }

    save ( draft : Draft ) : void
    {
        this.adapter.save ( draft )
    }

    setDateSquare ( millis : number ): void
    {
        this.adapter.setDateSquare(millis);
    }

    signOut(): void
    {
        this.adapter.signOut();
        this.updateBlockstack(BlockstackModel.signedOut());
    }
}
