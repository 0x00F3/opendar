export interface ILogger
{
    log: (message: string) => void;
}

export class Logger implements ILogger
{
    log: (message: string) => void;
    constructor(logFn: (message: string) => void)
    {
        this.log = logFn;
    }
}

