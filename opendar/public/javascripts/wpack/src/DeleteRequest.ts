export class DeleteRequest
{
    readonly id : string = "";
    readonly parent : string = "";
    readonly itemType : string = "";
}
