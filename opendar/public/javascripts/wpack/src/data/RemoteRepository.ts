import { Group } from '../models/Group';
import { ILogger } from '../core/Logger';
import { UserSession } from 'blockstack';


export class RemoteRepository
{
    readonly logger : ILogger;
    private readonly session:UserSession;
    constructor (logger: ILogger)
    {
        this.logger = logger;
        this.session = new UserSession();
    }

    //onAuthenticated is a function which will be called after
    //authenticated, and that may or may not happen.
    //Should this just return a promise of a string? What's cleaner?
    //--eholley 2019-08-27
    authenticate(onAuthenticated: (username:string) => void)
    {
        if(this.session.isUserSignedIn())
        {
            this.logger.log('User is signed in to session.');
            onAuthenticated(this.session.loadUserData().username);
            
        }
        else if (this.session.isSignInPending())
        {
            this.logger.log('Sign-in is pending.');
            this.session.handlePendingSignIn()
                .then((userData) => {
                    onAuthenticated(userData.username)
                });
        }
        this.logger.log('exiting "authenticate()".');
    }

    fetchGroups(): Promise<Array<Group>>
    {
        return this.session.getFile('group.json', { decrypt: true })
            .then((content) => {
                if (content === null)
                {
                    console.log('no groups on server.');
                    return new Array<Group>();
                }
                else
                {
                    
                    console.log('fetched content: ');
                    console.log(content.toString());
                    return new Array<Group>();
                }
            });
    }
    
    redirectToSignIn()
    {
        this.session.redirectToSignIn();
    }

    //wtf is this string??
    saveGroups(groups: Array<Group>): Promise<string>
    {
        return this.session
            .putFile('group.json', JSON.stringify(groups), { encrypt: true })
    }

    signOut()
    {
        this.session.signUserOut();
    }
}
