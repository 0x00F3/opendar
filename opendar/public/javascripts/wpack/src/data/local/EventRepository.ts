import { Event } from '../../models/Event';
import { IndexedDatabase } from './IndexedDatabase';

export class EventRepository
{
    private readwrite() : Promise<IDBObjectStore>
    {
        return IndexedDatabase.getInstance().idb
            .then(db => db.transaction( ['event'], 'readwrite') )
            .then(transaction => transaction.objectStore('event'));
    }

    delete( id : string ) : Promise<void>
    {
        return this.readwrite().then(objStore => objStore.delete(id))
            .then(req => new Promise<void>((resolve, reject) => {
                req.onsuccess = event => { resolve() };
            }));
    }

    
    getAll () :  Promise<Array<Event>>
    {
        return this.readwrite().then(objStore => objStore.getAll())
            .then(req => new Promise<Array<Event>>((resolve, reject) => {
                req.onsuccess = event => {
                    const result : Array<Event> =
                        <Array<Event>>req.result;
                    if (result.length > 0)
                    {
                        console.log('got events: ' + JSON.stringify(result));
                    }
                    resolve(<Array<Event>>req.result)
                };
            }));
    }  


    put (event : Event) : Promise<void>
    {
        console.log('saving event: ' + JSON.stringify(event));
        return this.readwrite()
            .then(objStore => new Promise<void>((resolve, reject) => {
                objStore.add(event).onsuccess = () => { resolve() }
            }));
    }
}
