import { Group } from '../../models/Group';
import { IndexedDatabase } from './IndexedDatabase';
import { Option } from '../../Option';

export class GroupRepository
{
    readwrite() : Promise<IDBObjectStore>
    {
        return IndexedDatabase.getInstance().idb
            .then(db => db.transaction(['group'], "readwrite"))
            .then(transaction => transaction.objectStore('group'));
    }

    create (group : Group) : Promise<void>
    {
        return this.readwrite()
            .then(objStore => new Promise<void>((resolve, reject) => {
                      objStore.add(group).onsuccess = () => { resolve() }
            }));
    }

    get(id : string) : Promise<Option<Group>>
    {
        return this.readwrite().then(objStore => objStore.get(id))
            .then(req => new Promise<Group> ((resolve, reject) => {
                req.onsuccess = event => { resolve(<Group | undefined>req.result) };
            }))
            .then(result => new Option<Group>(result));
    }

    getAll () : Promise<Array<Group>>
    {
        return this.readwrite().then(objStore => objStore.getAll())
            .then(req => new Promise<Array<Group>>((resolve, reject) => {
                req.onsuccess = event => { resolve(<Array<Group>>req.result) };
            }));
    }

    /* updates local repository where necessary (based on `updated`)
       and returns the merged list of most recent Groups */
    reconcile(remotes: Array<Group>): Promise<Array<Group>>
    {
        return Promise.all(remotes.map(remote => this.mostRecent(remote)));
    }

    /*--------------------helpers-----------------------*/
    /* given a copy of a group, returns the more recent of that copy
       or the one in the database. */
    private mostRecent(group: Group): Promise<Group>
    {
        return this.get(group.id)
            .then(opt => opt.map(g => g.updated < group.updated ? group : g))
            .then(option => option.getOrElse(group));
    }
}
