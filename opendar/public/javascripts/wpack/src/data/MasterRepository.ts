import { EventRepository } from './local/EventRepository';
import { GroupRepository } from './local/GroupRepository';
import { ILogger } from '../core/Logger';
import { RemoteRepository } from './RemoteRepository';
import { TodoRepository} from './local/TodoRepository';

export class MasterRepository
{
    readonly events: EventRepository;
    readonly groups: GroupRepository;
    readonly logger: ILogger;
    readonly remotes: RemoteRepository;
    readonly todos: TodoRepository;

    constructor(logger: ILogger)
    {
        this.events = new EventRepository();
        this.groups = new GroupRepository();
        this.logger = logger
        this.remotes = new RemoteRepository(logger);
        this.todos = new TodoRepository();
    }

    refreshRemote(): void
    {
        this.logger.log('refreshing groups...');
        this.remotes.fetchGroups()
            .then(remotes => this.groups.reconcile(remotes))
            .then(recents => this.remotes.saveGroups(recents));
        this.logger.log('refreshed groups.');
    }
        
}
