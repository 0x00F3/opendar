/* rough equivalent of Scala Option monad. 
 * needs help with naming. */

export class Option<T> {
    private readonly state: T | null;

    constructor(initial: T | null)
    {
        this.state = initial;
    }

    flatmap<TNext>(f: (x: T) => Option<TNext>): Option<TNext>
    {
        const intermediate:Option<Option<TNext>> = this.map(f);
        const result: Option<TNext>
            = intermediate.getOrElse(new Option(<TNext | null> null));
        return result;
    }

    getOrElse(fallback: T): T
    {
        return this.isEmpty() ? fallback : <T>this.state;
    }

    isEmpty() : boolean
    {
        return this.state === null || this.state === undefined;
    }
    
    map<TNext>(f: (x: T) => TNext | null): Option<TNext>
    {
        if(this.isEmpty())
        {
            return new Option(<TNext | null>null)
        }
        else return new Option(f(<T>this.state));
    }
/*
    static cast<T>(obj: Object): Option<T>
    {
        if(obj instanceof T) return new Option(<T> obj);
        else return new Option(<T | null> null);
    } */
}
