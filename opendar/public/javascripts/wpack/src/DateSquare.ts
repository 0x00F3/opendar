import { ImDate } from './ImDate';
import { MasterRepository } from './data/MasterRepository';
import { Occurrence } from './Occurrence';

export class DateSquare
{
    date: number; //1-31. Calculating this in elm causes DST problems.
    millis : number; //millisecond when day began.
    local : string; //long-form representation of the date in locale
    occurrences : Array<Occurrence>;

    private static instance : Promise<DateSquare> | null = null;

    private constructor ( date: number
                          , local : string
                          , millis : number
                          , occurrences: Array<Occurrence>
                        )
    {
        this.date = date;
        this.occurrences = occurrences;
        this.local = local; // do I need this still? consider deleting.
        this.millis = millis;
    }

    static fromDate(imDate : ImDate,
                    master: MasterRepository) : Promise<DateSquare>
    {
        const options = { weekday: 'long',
                          year: 'numeric',
                          month: 'long',
                          day: '2-digit'
                        };
        return Occurrence.fromDate(imDate, master)
            .then(os => new DateSquare(imDate.toDate().getDate(),
                                       imDate.format(options),
                                       imDate.toDate().getTime(),
                                       os
                                      ));
    }

    private static getInstance(master: MasterRepository): Promise<DateSquare>
    {
        if ( DateSquare.instance === null )
        {
            DateSquare.instance = DateSquare.initialize(master);
        }
        return <Promise<DateSquare>>DateSquare.instance;
    }

    private static initialize(master: MasterRepository) : Promise<DateSquare>
    {
        return DateSquare.fromDate(ImDate.now(), master);
    }

    public static latest(master: MasterRepository) : Promise<DateSquare>
    {
        DateSquare.instance = DateSquare.getInstance(master)
            .then(dsq => new ImDate(new Date(dsq.millis)))
            .then(imDate => DateSquare.fromDate(imDate, master));
        return DateSquare.getInstance(master);
    }

    public static set(millis: number,
                      master: MasterRepository): Promise<DateSquare>
    {
        DateSquare.instance =
            DateSquare.fromDate(new ImDate(new Date(millis)), master);
        return DateSquare.getInstance(master);
    }
}
