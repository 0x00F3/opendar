import { DateSquare } from './DateSquare';
import { ImDate } from './ImDate';
import { MasterRepository } from './data/MasterRepository';

export class Calendar
{
    readonly master: MasterRepository
    month : number;
    year : number;
    weeks: Array<Array<DateSquare>>;

    private constructor(master: MasterRepository,
                        month : number,
                        year : number,
                        weeks: Array<Array<DateSquare>>)
    {
        this.master = master
        this.month = month;
        this.year = year;
        this.weeks = weeks;
    }

    private static instance : Promise<Calendar> | null = null;

    /* advances the instance calendar one month and returns that same calendar.*/
    public static advance(master: MasterRepository) : Promise<Calendar>
    {
        const result = Calendar.getInstance(master).then(cal => {
            const month = cal.month === 11 ? 0 : cal.month + 1;
            const year = cal.month === 11 ? cal.year + 1 : cal.year;
            return Calendar.from(cal.master, month, year);
        });

        Calendar.instance = result;
        return result;
    }

    public static decrease(master: MasterRepository) : Promise<Calendar>
    {
        const result = Calendar.getInstance(master).then(cal => {
            const month = cal.month === 0 ? 11 : cal.month - 1;
            const year = cal.month === 0 ? cal.year - 1 : cal.year;
            return Calendar.from(cal.master, month, year);
        });

        Calendar.instance = result;
        return result;
    }

    
    private static from(master: MasterRepository,
                        month: number,
                        year: number): Promise<Calendar>
    {
        console.log('initializing calendar with ' + month + ' and '
                    + year + '...');
        const firstDisplayDay =
            //declaring here that Monday is the first day of the week. 
            ImDate.firstDisplayDate(1, month, year);

        console.log('firstDisplayDay: ' + firstDisplayDay.toDate().toLocaleString());
        const weeks : Promise<Array<Array<DateSquare>>> =
            Calendar.weeksStartingWith(master, firstDisplayDay);
        const result = weeks.then(ws => new Calendar(master, month, year, ws))
            .catch(e => console.log('error in calendar instantiation: '
                                    + JSON.stringify(e.message)));
        return <Promise<Calendar>>result;
    }
    
    private static getInstance(master: MasterRepository) : Promise<Calendar>
    {
        console.log('getting Calendar instance...');
        if ( Calendar.instance === null )
        {
            Calendar.instance = Calendar.initialize(master);
        }

        (<Promise<Calendar>>Calendar.instance)
            .then(cal => console.log('got calendar: ' + JSON.stringify(cal)))
            .catch(e => console.log('error getting calendar: ' + e));
        
        return <Promise<Calendar>>Calendar.instance;    
    }

    /* refreshes instance calendar and returns that calendar.*/
    public static latest(master: MasterRepository) : Promise<Calendar>
    {
        console.log('getting latest Calendar...');
        const result = Calendar.getInstance(master)
            .then(cal => Calendar.from(cal.master, cal.month, cal.year));
        Calendar.instance = result;
        return result;

    }
    /*-------------------------------------------------------------*/
/*
    static getMonthOfDates(month: number, year : number): Promise<Array<DateSquare>>
    {
        return Calendar.getMonthOfDatesHelper(new ImDate(new Date(year, month, 1))
                                       , month
                                       , new Promise((resolve, reject) => resolve([])));
    }
*/
    /*
    private static getMonthOfDatesHelper(imDate : ImDate,
                                         month : number,
                                         result : Promise<Array<DateSquare>>)
        : Promise<Array<DateSquare>>
    {
        if (imDate.getMonth() === month)
        {
            const array : Promise<Array<DateSquare>> = DateSquare.fromDate(imDate)
                .then(ds => result.then(arr => arr.concat([ds])))
            return Calendar.getMonthOfDatesHelper(imDate.next(), month, array);
        }
        else
        {
            return result;
        }
    }
*/
    private static initialize(master: MasterRepository): Promise<Calendar>
    {
        return Calendar.from(master,
                             ImDate.now().getMonth(),
                             ImDate.now().getFullYear()
                            );
    }

    private static weekStartingWith(master: MasterRepository,
                                    imDate: ImDate,
                                    result: Promise<Array<DateSquare>>
                                    = new Promise((res, rej) => res([]))
                                   )
    :Promise<Array<DateSquare>>
    {
//        console.log('weekStartingWith with imDate: ' + imDate.toDate().toLocaleString());
        return result.then(r => {
            return r.length >= 7
                ? new Promise ((res, rej) => res(r))
                : DateSquare.fromDate(imDate, master)
                .then(ds => Calendar.weekStartingWith(
                    master,
                    imDate.next(),
                    new Promise((res, rej) => res(r.concat([ ds ])))
                ))
        });
    }

    private static weeksStartingWith(master: MasterRepository,
                                     imDate: ImDate,
                                     result: Promise<Array<Array<DateSquare>>>
                                     = new Promise((res, rej) => res([])))
    : Promise<Array<Array<DateSquare>>>
    {
        console.log('creating week starting with: ' + imDate.toDate().toLocaleString());
        return result.then(r => r.length >= 6
                           ? new Promise((res, rej) => res(r))
                           : Calendar.weekStartingWith(
                               master,
                               imDate.withDate(imDate.getDate())
                           ).then(week => r.concat( [ week ] ))
                           .then(weeks =>
                                 Calendar.weeksStartingWith(master,
                                                            imDate.withDate(
                                                                imDate.getDate()
                                                                    + 7
                                                            )
                                                            , new Promise((res, rej) =>
                                                                          res(weeks)
                                                                         )
                                                           )
                                )
                          );
    }
}


