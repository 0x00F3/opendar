import { ElmActions } from './src/ElmActions';

window.addEventListener('load', () => {
    navigator.serviceWorker.register('/sw.js');
    console.log('ServiceWorker registration successful.');
}, err => console.log('ServiceWorker registration failed: ', err));


const app = Elm.Main.init({ node: document.getElementById('elm-target') });

const actions = new ElmActions();

actions.debugLog = message => console.log(message);
//app.ports.debugLog.send(message); //logs to screen.

actions.updateBlockstack = model => app.ports.updateBlockstack.send(model);

actions.updateCalendar = calendar => app.ports.updateCalendar.send(calendar);
actions.updateDateSquare = dsq => app.ports.updateDateSquare.send(dsq);
                
actions.updateGroups = groups => app.ports.updateGroups.send(groups);
actions.updateTodos = todos => app.ports.updateTodos.send(todos);

app.ports.command.subscribe(command => {
    actions.command(command);
});

app.ports.deleteItem.subscribe(deleteRequest => {
    actions.deleteItem(deleteRequest);
});

app.ports.deleteTodo.subscribe(id => {
    actions.deleteTodo(id);
    console.log('deleting todo: ' + id);
    console.log('');
});

app.ports.save.subscribe(draft => {
    console.log('saving draft: ' + JSON.stringify(draft));
    actions.save(draft);
});

app.ports.setDateSquare.subscribe(millis => actions.setDateSquare(millis));

actions.initialize();
