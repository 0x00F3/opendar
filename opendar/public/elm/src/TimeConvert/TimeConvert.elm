module TimeConvert exposing ( convert ) 

import PosixOps
import Time exposing ( Month ( .. ), Zone, Posix )

{- given a timezone and a year, month, day, hour, minute and second, returns
   a Posix that matches those dates inside that timezone. This is the only
   method in this module that should be exposed.
-}
convert : Zone -> Int -> Month -> Int -> Int -> Int -> Int -> Posix
convert zone year month day hour minute second =
    matchYear zone year seed
        |> matchMonth zone month
        |> matchDay zone day
        |> matchHour zone hour
        |> matchMinute zone minute
        |> matchSecond zone second

{- given an initial posix at an arbitrary day, returns a new posix within the 
   same initial year and month that matches the target day.
-}
matchDay : Zone -> Int -> Posix -> Posix
matchDay zone targetDay initial =
    if Time.toDay zone initial == targetDay then
        initial
    else
        targetDay - Time.toDay zone initial
            |> PosixOps.addShortDays initial
            |> matchDay zone targetDay

{- given an initial posix at an arbitrary hour, returns a new posix within the 
   same initial year, month, and day that matches the target hour.
-}
matchHour : Zone -> Int -> Posix -> Posix
matchHour zone targetHour initial =
    if Time.toHour zone initial == targetHour then
        initial
    else targetHour - Time.toHour zone initial
        |> PosixOps.addShortHours initial
        |> matchHour zone targetHour

{- given an initial posix at an arbitrary minute, returns a new posix within 
   the same initial year, month, day, and hour that matches the target
   minute.
-}
matchMinute : Zone -> Int -> Posix -> Posix
matchMinute zone targetMinute initial =
    if Time.toMinute zone initial == targetMinute then
        initial
    else targetMinute - Time.toMinute zone initial
        |> PosixOps.addShortMinutes initial
        |> matchMinute zone targetMinute

{- given an initial posix at an arbitrary month, returns a new posix within 
   the same initial year that matches the target
   month
-}
matchMonth : Zone -> Month -> Posix -> Posix
matchMonth zone targetMonth initial =
    matchNumericMonth zone ( toNumericMonth targetMonth ) initial

{- helper method for matchMonth -}
matchNumericMonth : Zone -> Int -> Posix -> Posix
matchNumericMonth zone targetMonth initial =
    if toNumericMonth ( Time.toMonth zone  initial ) == targetMonth then
        initial
    else
        targetMonth - toNumericMonth ( Time.toMonth zone initial )
            |> PosixOps.addShortMonths initial
            |> matchNumericMonth zone targetMonth

{- given an initial posix at an arbitrary second, returns a new posix within 
   the same initial year, month, day, hour, and that matches the target
   second.
-}
matchSecond : Zone -> Int -> Posix -> Posix
matchSecond zone targetSecond initial =
    if Time.toSecond zone initial == targetSecond then
        initial
    else
        targetSecond - Time.toSecond zone initial
            |> PosixOps.addSeconds initial
            |> matchSecond zone targetSecond

{- given an arbitrary initial posix , returns a new posix within that matches 
   the target year.
-}
matchYear : Zone -> Int -> Posix -> Posix
matchYear zone targetYear initial = 
    if Time.toYear zone initial == targetYear then
        initial
    else
        targetYear - Time.toYear zone initial
            |> PosixOps.addShortYears initial
            |> matchYear zone targetYear

{- an arbitrary seed for this conversion algorithm. Happens to be 1970-01-01. 
-}
seed : Posix
seed = Time.millisToPosix 0


toNumericMonth : Month -> Int
toNumericMonth month = 
    case month of
        Jan -> 1
        Feb -> 2
        Mar -> 3
        Apr -> 4
        May -> 5
        Jun -> 6
        Jul -> 7
        Aug -> 8
        Sep -> 9
        Oct -> 10
        Nov -> 11
        Dec -> 12
