module PosixOps exposing (..)

import Minimum
import Time exposing (Posix)

addSeconds : Posix -> Int -> Posix
addSeconds initial addend =
    Time.posixToMillis initial
        |> \n -> n + addend * 1000
        |> Time.millisToPosix

{- a "short day" means an arbitrary amount of time greater than an hour but
   less than a day.-}
addShortDays : Posix -> Int -> Posix
addShortDays initial addend =
    Minimum.secondsInDay * addend |> addSeconds initial

{- a "short hour" means an arbitrary amount of time greater than a minute but
   less than an hour-}
addShortHours : Posix -> Int -> Posix
addShortHours initial addend =
    Minimum.secondsInHour * addend |> addSeconds initial

{- a "short minute" means an arbitrary amount of time greater than a second 
   but less than an hour -}
addShortMinutes initial addend =
    Minimum.secondsInMinute * addend |> addSeconds initial

{- a "short month" means an arbitrary amount of time greater than a day but
   less than a month -}
addShortMonths : Posix -> Int -> Posix
addShortMonths initial addend =
    Minimum.secondsInMonth * addend |> addSeconds initial


{- a "short year" is an arbitrary amount of time greater than a month but
   less than a year -}
addShortYears : Posix -> Int -> Posix
addShortYears initial addend =
    Minimum.secondsInYear * addend |> addSeconds initial
