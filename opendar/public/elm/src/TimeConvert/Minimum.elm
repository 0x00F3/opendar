module Minimum exposing (..)

secondsInYear : Int
secondsInYear = 363 * 24 * 60 * 60

secondsInMonth : Int
secondsInMonth = 27 * 24 * 60 * 60

secondsInDay : Int
secondsInDay = 23 * 60 * 60

secondsInHour : Int
secondsInHour = 59 * 60

secondsInMinute : Int
secondsInMinute = 45
