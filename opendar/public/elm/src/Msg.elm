module Msg exposing (..)

import Context exposing ( Context )
import DateSquare exposing ( DateSquare )
import DeleteRequest exposing ( DeleteRequest )
import Draft exposing ( Draft )
import Json.Encode exposing ( Value )

type Msg = AdvanceMonth
    | CloseEditor
    | CloseSecondaryPane
    | CloseTertiaryPane
    | DebugLog String --never do this. 
    | DecreaseMonth
    | DeleteItem DeleteRequest
    | DeleteTodo String
    | Initialize Context
    | OpenEditor
    | OpenEditorForMillis Int
    | OpenDatePane DateSquare
    | OpenProfilePane
    | OpenTodosPanel
    | RedirectToSignIn
    | Save
    | SetDescription String
    | SetEndDate String
    | SetEndTime String
    | SetGroup String
    | SetGroupColor String
    | SetGroupName String
    | SetLocation String
    | SetNotes String
    | SetStartDate String
    | SetStartTime String
    | SignOut
    | UpdateBlockstack Value
    | UpdateCalendar Value
    | UpdateDateSquare Value
    | UpdateGroups Value
    | UpdateTodos Value
