{- exposes functions for navigating around a calendar -}
module CalNav exposing (..)

import Day
import Time exposing ( Month (..), Posix, Weekday (..), Zone )
import TimeConvert

addDays : Int -> Posix -> Posix
addDays n initial = addSeconds ( secondsPerDay * n) initial

addSeconds : Int -> Posix -> Posix
addSeconds addend initial =
    Time.posixToMillis initial
        |> \n -> addend * 1000 + n
        |> Time.millisToPosix

advanceMonth : Month -> Month
advanceMonth month =
    case month of
        Jan -> Feb
        Feb -> Mar
        Mar -> Apr
        Apr -> May
        May -> Jun
        Jun -> Jul
        Jul -> Aug
        Aug -> Sep
        Sep -> Oct
        Oct -> Nov
        Nov -> Dec
        Dec -> Jan

decreaseMonth : Month -> Month
decreaseMonth month =
    case month of
        Jan -> Dec
        Feb -> Jan
        Mar -> Feb
        Apr -> Mar
        May -> Apr
        Jun -> May
        Jul -> Jun
        Aug -> Jul
        Sep -> Aug
        Oct -> Sep
        Nov -> Oct
        Dec -> Nov

               
displayMonth : Zone -> Month -> Int -> List ( List Posix )
displayMonth zone month year = 
    weeksStartingWith 6 ( firstDisplayDay zone month year)

firstDayOfMonth : Zone -> Month -> Int -> Posix
firstDayOfMonth zone month year =
    TimeConvert.convert zone year month 1 0 0 0

firstDayOfWeek : Weekday
firstDayOfWeek = Mon
                 
{- for example, if weeks start on a Monday, but June starts on a Saturday, 
   it will return May 27, because that's the first day that will be displayed
   for that June
-} 
firstDisplayDay : Zone -> Month -> Int -> Posix
firstDisplayDay zone month year =
    mostRecentFirstDay zone (firstDayOfMonth zone month year)
        
{- given the initial Posix, returns that day if it's the first day of the week
   or the most recent first day of the week -}
mostRecentFirstDay : Zone -> Posix -> Posix
mostRecentFirstDay zone initial =
    if Time.toWeekday zone initial == firstDayOfWeek then
        initial
    else mostRecentFirstDay zone (addDays -1 initial)

secondsPerDay : Int
secondsPerDay = 86400

{- gets seven days starting with the initial day -}
weekStartingWith : Posix -> List Posix
weekStartingWith initial =
    weekStartingWithHelper [ initial] ( addDays 1 initial ) 

--do not expose
weekStartingWithHelper : List Posix -> Posix-> List Posix
weekStartingWithHelper list next =
    if List.length list >= 7 then list
    else weekStartingWithHelper ( list ++ [ next ] ) ( addDays 1 next )
                
weeksStartingWith : Int -> Posix -> List (List Posix)
weeksStartingWith numWeeks firstDay =
    weeksStartingWithHelper numWeeks firstDay []

weeksStartingWithHelper : Int -> Posix -> List(List Posix) -> List (List Posix)
weeksStartingWithHelper numWeeks seed list =
    if List.length list >= numWeeks then list
    else
        list ++ [ weekStartingWith seed ]
        |> weeksStartingWithHelper numWeeks (addDays 7 seed)
