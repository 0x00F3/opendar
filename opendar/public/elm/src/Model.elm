module Model exposing (..)

import Blockstack exposing ( Blockstack )
import Cal exposing ( Cal )
import Context exposing ( Context )
import DateSquare exposing ( DateSquare )
import DeleteRequest exposing ( DeleteRequest )
import Draft exposing (..)
import Event exposing ( Event )
import Group exposing ( Group )
import Occurrence exposing (..)
import Time exposing (..)
import Todo exposing ( Todo ) 

type alias Model =
    { blockstack : Blockstack
    , cal : Cal
    , debug : String
    , context : Maybe Context
    , dateSquare : DateSquare
    , draft : Draft
    , events : List Event
    , groups : List Group
    , occurrences : List Occurrence
    , secondaryPanel : SecondaryPane
    , tertiaryPanel : TertiaryPane
    , todos : List Todo
    }

type SecondaryPane =
    SecondaryClosed
        | DatePane
        | TodosPanel

type TertiaryPane = TertiaryClosed
    | Editor
    | Profile

type Panel = Primary | Secondary | Tertiary

deleteItem : Model -> DeleteRequest -> Model
deleteItem model request =
    model.dateSquare.occurrences
        |> List.filter ( \o -> o.draft.id /= request.id )
        |> updateOccurrences model

firstPanelClass : Model -> String
firstPanelClass model =
    if model.tertiaryPanel /= TertiaryClosed then
        if model.secondaryPanel /= SecondaryClosed then
            ""
        else "focus-secondary"
    else -- tertiary is closed
        if model.secondaryPanel /= SecondaryClosed then
            "focus-secondary"
        else "focus-primary"

initialize : Model
initialize =
    { blockstack = Blockstack.initialize
    , cal = Cal.initialize
    , context = Nothing
    , dateSquare = DateSquare.initialize
    , debug = "debug log <br />"
    , draft = Draft.initialize
    , groups = Group.default
    , events = []
    , occurrences = []
    , secondaryPanel = SecondaryClosed
    , tertiaryPanel = TertiaryClosed
    , todos = []
    }
            
secondPanelClass model =
    if model.tertiaryPanel /= TertiaryClosed then
        "focus-secondary"
    else "focus-primary"

updateOccurrences : Model -> List Occurrence -> Model
updateOccurrences model occurrences =
    model.dateSquare
        |> \dateSquare -> { dateSquare | occurrences = occurrences }
        |> \ds -> { model | dateSquare = ds }
