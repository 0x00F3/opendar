module Main exposing (main)

import Blockstack exposing ( Blockstack )
import Browser
import DateSquare exposing ( DateSquare )
import Draft exposing (..)
import Cal exposing ( Cal )
import Context exposing ( Context )
import CalendarViews --exposing (monthTable)
import CalNav
import Editor exposing (..)
import Event exposing ( Event )
import Group exposing ( Group )
import Html exposing (..)
import Html.Attributes exposing (..)
import Json.Decode exposing ( list )
import Model exposing (..)
import Msg exposing (..)
import Ports
import Structure
import Task exposing (..)
import Time exposing (..)
import Todo exposing ( Todo )
import Toolbar exposing (..)

main : Program () Model Msg
main = Browser.element
       { init = init
       , subscriptions = subscriptions
       , update = update
       , view = view
       }

---------------------------------------

init : () -> ( Model, Cmd Msg)
init _ =
    ( Model.initialize
    , Task.map2 Context.initialize Time.here Time.now
        |> Task.perform Initialize
    )

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Ports.debugLog DebugLog
        , Ports.updateBlockstack UpdateBlockstack
        , Ports.updateCalendar UpdateCalendar
        , Ports.updateDateSquare UpdateDateSquare
        , Ports.updateGroups UpdateGroups
        , Ports.updateTodos UpdateTodos
        ]

view : Model -> Html Msg
view model = Structure.view model

    
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AdvanceMonth ->        
            ( model, Ports.command "advanceMonth" )
        CloseEditor ->
            ( { model | tertiaryPanel = TertiaryClosed }, Cmd.none )
        CloseSecondaryPane ->
            ( { model | secondaryPanel = SecondaryClosed }, Cmd.none )
        CloseTertiaryPane ->
            ( { model | tertiaryPanel = TertiaryClosed }, Cmd.none )

        DebugLog message ->
            ( { model | debug = model.debug ++ message ++ "<br />\n" }, Cmd.none)
        DecreaseMonth ->
            ( model, Ports.command "decreaseMonth" )
        DeleteItem request -> 
            ( Model.deleteItem model request, Ports.deleteItem request )
        DeleteTodo id ->
            ( model, Ports.deleteTodo id )
        Initialize calendar ->
            ( { model | context = Just calendar }, Cmd.none )
        OpenDatePane sq ->
            ( { model | secondaryPanel = DatePane }
            , Ports.setDateSquare sq.millis )
        OpenTodosPanel ->
            ( { model | secondaryPanel = TodosPanel }, Cmd.none )
        OpenEditor -> 
            ( { model | tertiaryPanel = Editor
              , draft = Draft.initialize
              }
            , Cmd.none
            )
        OpenEditorForMillis millis ->
            case model.context of
                Just context -> 
                    ( { model | draft = Draft.fromStartMillis context.here millis
                      , tertiaryPanel = Editor
                      }
                    , Cmd.none)
                Nothing -> ( { model | tertiaryPanel = Editor
                             , draft = Draft.initialize
                             }
                           , Cmd.none
                           )
        OpenProfilePane ->
            ( { model | tertiaryPanel = Profile}, Cmd.none  )
        RedirectToSignIn ->
            ( model, Ports.command "redirectToSignIn" )
        Save ->
            { model | tertiaryPanel = TertiaryClosed }
                |> \m -> ( m, Ports.save m.draft )
        SetDescription description ->
            model.draft
                |> \draft -> { draft | description = description }
                |> \d -> ( { model | draft = d }, Cmd.none )
        SetEndDate date ->
            model.draft
                |> \draft -> { draft | endDate = date }
                |> \d -> ( { model | draft = d }, Cmd.none )
        SetEndTime time ->
            model.draft
                |> \draft -> { draft | endTime = time }
                |> \d -> ( { model | draft = d }, Cmd.none )
        SetGroup id ->
            model.draft
                |> \draft -> { draft | groupId = id }
                |> \d -> ( { model | draft = d }, Cmd.none )
        SetGroupColor hex ->
            model.draft
                |> \draft -> { draft | groupColor = hex }
                |> \d -> ( { model | draft = d }, Cmd.none )
        SetGroupName name ->
            model.draft
                |> \draft -> { draft | groupName = name, groupId = "" }
                |> \d -> ( { model | draft = d }, Cmd.none )
        SetLocation location ->
            model.draft
                |> \draft -> { draft | location = location }
                |> \d -> ( { model | draft = d }, Cmd.none )
        SetNotes notes ->
            model.draft
                |> \draft -> { draft | notes = notes }
                |> \d -> ( { model | draft = d }, Cmd.none )
        SetStartDate date ->
            model.draft
                |> \draft -> { draft | startDate = date }
                |> \d -> ( { model | draft = d }, Cmd.none )
        SetStartTime time ->
            model.draft
                |> \draft -> { draft | startTime = time }
                |> \d -> ( { model | draft = d }, Cmd.none )
        SignOut ->
            ( model, Ports.command "signOut" )
        UpdateBlockstack value ->
            case Json.Decode.decodeValue Blockstack.decoder value of
                Ok user -> ( { model | blockstack = user }, Cmd.none)
                Err error -> ( model, Cmd.none )
        UpdateCalendar value ->
            case Json.Decode.decodeValue Cal.decoder value of
                Ok cal -> ( { model | cal = cal }, Cmd.none )
                Err error -> ( model, Cmd.none )
        UpdateDateSquare value ->
            case Json.Decode.decodeValue (DateSquare.decoder) value of
                Ok dsq ->
                    ( { model | dateSquare = dsq
                      }, Cmd.none )
                Err error -> ( model, Cmd.none )
        UpdateGroups value -> 
            case Json.Decode.decodeValue (Json.Decode.list Group.groupDecoder) value of
                Ok groups ->
                    ( { model | groups =
                        List.sortBy (\e-> e.updated) (Group.default ++ groups)}
                      , Cmd.none
                      )
                Err error ->
                    ( model, Cmd.none )
        UpdateTodos value ->
            case Json.Decode.decodeValue ( Json.Decode.list Todo.todoDecoder ) value of
                Ok todos ->
                          ( { model | todos =
                                  List.sortBy(\t-> t.updated) todos
                            }, Cmd.none )
                Err error -> ( model, Cmd.none )
