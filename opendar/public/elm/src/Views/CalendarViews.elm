module CalendarViews exposing ( pane )

import Cal exposing ( Cal )
import Context exposing ( Context )
import CalendarUtils
import DateSquare exposing ( DateSquare )
import DayView
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model exposing ( Model )
import Msg exposing (..)
import Time exposing ( Month (..), Posix, Zone )

dayNames : List (Html Msg)
dayNames =
    List.map (\day -> dayNamesCell day) CalendarUtils.weekdayOrder
dayNamesCell : Time.Weekday -> Html Msg
dayNamesCell day =
    div[ class "day-name flex-1" ] [ text (CalendarUtils.toShortDay day) ]
                                 
        
header : Cal -> String
header cal =
    " " ++(CalendarUtils.toLocalMonth cal.month)
        ++ " "
        ++ String.fromInt cal.year
        ++ " " 


monthTable : Model -> Context -> Html Msg
monthTable  model context =
    div [ class "flex-column full-width grow-1 month" ]
        [ div [ class "table-header month-header" ]
              [ div [ class "month-row" ]
                    [ div [ class "centered full-width" ]
                         [ button [ class "btn btn-default"
                                  , onClick DecreaseMonth
                                  ]
                               [ text "←" ]
                         , text ( header model.cal )
                         , button [ class "btn btn-default"
                                  , onClick AdvanceMonth
                                  ]
                             [ text "→" ]
                         ]
                    ] -- end th
              , div [ class "flex-row" ] dayNames
              ] -- end tr
        , div [ class "flex-column grow-1 month-body" ]
            ( List.map
                  (\week -> weekRow model context week)
                      model.cal.weeks
            )
        ] -- end table

        
weekRow : Model-> Context -> List DateSquare -> Html Msg
weekRow model context list =
    div [ class "flex-row grow-1 month-row" ]
        (List.map (\day -> DayView.square model context day) list)

pane : Model -> Context -> Html Msg
pane model calendar =
    div [ class "flex-column grow-1"]
        [ monthTable model calendar
        , span [ id "debug-target" ][text model.debug]
        ]
