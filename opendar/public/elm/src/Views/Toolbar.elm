module Toolbar exposing ( toolbar )

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model exposing ( Model )
import Msg exposing (..)

blockstackStatusButton : Model -> Html Msg
blockstackStatusButton model =
    if model.blockstack.isSignedIn
    then button [ class "btn btn-default"
                , onClick OpenProfilePane
                , title ("You are signed in as " ++ model.blockstack.username)]
        [ text "😃"
        ]
    else button [ class "btn btn-default"
                , onClick OpenProfilePane
                , title "⚠️You are not signed in."
                ]
        [ text "😕"
        ]

openTodosPanelButton : Html Msg
openTodosPanelButton =
    button [ class "btn btn-default", onClick OpenTodosPanel]
        [ text "📋" ]

toolbar : Model -> Html Msg
toolbar model =
    div [ class "flex-row with-pad" ]
        [ div [class "full-width right-aligned"]
          [ openTodosPanelButton
          , blockstackStatusButton model
          ]
        ]


        
