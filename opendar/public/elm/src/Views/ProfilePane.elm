{- a tertiary pane displaying either Blockstack information or a 
   log in button. 

   This might be scrap. It's not currently being used and might never be
   used.
-}

module ProfilePane exposing (pane)

import Blockstack exposing ( Blockstack )
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model exposing ( .. )
import Msg exposing (..)

pane : Model -> Html Msg
pane model =
    div [ class "flex-column focus-tertiary index-pane" ]
        [ div [class "flex-row"]
              [ div [class "grow-1"]
                    [ h2 []
                          [ text "Account Information" ]
                        ]
                    
                    , button [class "btn btn-default pull-right"
                             , onClick CloseTertiaryPane
                             ]
                    [ text "❌ "]
              ]
        , content model
        ]

content : Model -> Html Msg
content model = 
    if model.blockstack.isSignedIn
    then div [ class "signed-in" ]
        [ text "You are signed in as: "
        , strong [] [ text model.blockstack.username ]
        , br [][] 
        , input [ class "btn btn-primary"
                , onClick SignOut
                , type_ "button"
                , value "Sign out ➡"
                ][]
        ]
    else div [ class "not-signed-in" ]
        [ p []
              [ text "Opendar.io uses Blockstack.org for remote storage. If you "
              , text "would like, "
              , text "you may sign in to "
              , text "Blockstack and return to Opendar.io. "
              , text "Signing in is entirely optional. "
              ]
        , input [ class "btn btn-primary"
                , onClick RedirectToSignIn
                , type_ "button"
                , value "Sign in ➡"
                ][]
        ]
       
    
