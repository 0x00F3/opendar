module Editor exposing (view)

import Draft exposing ( Draft )
import Group exposing ( Group )
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model exposing (..)
import Msg exposing (..)

view : Model -> Html Msg
view model =
    Html.form [ class "flex-column"]
        [ div [ class "flex-row"]
              [ h2 [ class "grow-1"] [ text (header model.draft) ]
              , input [class "btn btn-default pull-right"
                      , onClick CloseEditor
                      , type_ "button"
                      , value "❌"
                      ] []
              ]
        , input [ class "textbox"
                , onInput SetDescription
                , placeholder "description"
                , type_ "text"
                , value model.draft.description
                ][]
        , label [ ] [ text "Start Date" ]
        , startDate model.draft
        , endDate model.draft
        , input [ class "textbox"
                , onInput SetLocation
                , placeholder "location"
                ,  type_ "text"
                , value model.draft.location
                ][]
        , textarea [ class "textbox"
                   , onInput SetNotes
                   , placeholder "notes"
                   , value model.draft.notes
                   ][]
        , groups model
        , input [ class "btn btn-primary"
                , onClick Save
                , type_ "button"
                , value "✔️"
                ][]
        ]

endDate : Draft -> Html Msg
endDate draft =
    if ( draft.startDate == "" ) then
        text ""
    else
        div []
            [ label [ ] [ text "End Date" ]
            , div [ class "flex-row" ]
                [ input [ class "textbox grow-1"
                        , type_ "date"
                        , onInput SetEndDate
                        , value draft.endDate
                        ] []
                , input [ class "textbox"
                        , type_ "time"
                        , onInput SetEndTime
                        , value draft.endTime
                        ] []
                ]
            ]


group : String -> Group -> Html Msg
group selected model =
    label [ class "list-item-border"
          , style "color" model.color
          , style "background-color" model.background
          ]
    [ input [ checked (model.id == selected )
            , name "group"
            , onInput SetGroup
            , type_ "radio"
            , value model.id
            ]
          []
    , text model.name
    ]

groupCreator : Draft -> Html Msg
groupCreator draft =
    div [ class "flex-row" ]
        [ input [ name "group"
                , type_ "radio"
                , onInput SetGroup
                , value "" --special value representing a new group from user
                , checked ( "" == draft.groupId )
                ][]
        , input [ class "flex-1 textbox"
                , onInput SetGroupName
                , placeholder "create..." 
                , type_ "text"
                , value draft.groupName
                ][]
        , div [ class "flex-column" ]
            [ input [ class "btn btn-default grow-1"
                    , onInput SetGroupColor
                    , type_ "color"
                    , value draft.groupColor
                    ][]
            ]
            
        ]
        
groups : Model -> Html Msg
groups model =
    div [ class "flex-column" ]
        ( [ h3 [] [ text "Groups" ] ]
              ++ ( List.map
                       (\g -> group model.draft.groupId g) model.groups
                 )
              ++ [ groupCreator model.draft ]
        )

header : Draft -> String
header draft =
    draft.state ++ " " ++ Draft.listItemCategory draft


startDate : Draft -> Html Msg
startDate draft =
    div [ class "flex-row" ]
        [ input [ class "textbox grow-1"
                , type_ "date"
                , id "start-date" --for testing
                , onInput SetStartDate
                , value draft.startDate
                ] []
        , input [ class "textbox"
                , type_ "time"
                , onInput SetStartTime
                , value draft.startTime
                ] []
        ]
