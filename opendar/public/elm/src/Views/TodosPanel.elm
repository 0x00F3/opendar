module TodosPanel exposing ( panel )

import Group exposing ( Group )
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model exposing (..)
import Msg exposing (..)
import Todo exposing ( Todo )

panel : Model -> Html Msg
panel model =
    div [ class "flex-column" ]
        [ div [class "flex-row"]
              [ div [class "grow-1"]
                    [ h2 [] [ text "Todos\u{00A0}" ]
                    , button [ class "btn btn-default", onClick OpenEditor ]
                        [text "➕"
                        ]
                    ]
                    , button [class "btn btn-default pull-right"
                             , onClick CloseSecondaryPane
                             ]
                    [ text "❌ "]
              ] 
        , todoList model
        ]
        
        
itemView : Todo -> Group -> Html Msg
itemView item group =
    div [ class "flex-row list-item-border"
        , style "color" group.color
        , style "background-color" group.background
        ]
        [ div [ class "grow-1 list-item" ]
              [ strong [] [ text item.description ]
              , br[][]
              , span [] [ text (if item.location == "" then "" else "🗺️" )]
              , span [] [ text item.location]
              , br[][]
              , text ( if item.notes == "" then "" else "📝")
              , em [][ text item.notes ]
              ]
        , button [ class "btn-embedded"
                 , onClick (DeleteTodo item.id)
                 --, type_ "button"
                 --, value "✔️"][]
                 , value item.id
                ][text "✔️"]
        ]
            
todoList : Model -> Html Msg
todoList model =
    ul [ class "list-items" ]
        ( List.map (\t -> itemView t (Group.find model.groups t.group) )
             model.todos)
        
         
    
