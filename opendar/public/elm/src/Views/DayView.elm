module DayView exposing ( square )

import Context exposing (..)
import CalendarUtils
import DateSquare exposing ( DateSquare )
import Group exposing ( Group )
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model exposing ( Model )
import Msg exposing (..)
import Occurrence exposing ( Occurrence )
import Time exposing ( Month (..), Posix, Zone )


{-I don't want to have to unwrap the Maybe Calendar again so I'm injecting it 
  here even though it's unnecessary -}
square : Model -> Context -> DateSquare -> Html Msg
square model context sq =
    div [ class "flex-1 flex-column day-square-background" ]
    [ div [ class "day-square grow-1", onClick (OpenDatePane sq) ]
          [ span [ class ( Time.millisToPosix sq.millis
                         |> numberClass model context) ]
                [ text (Time.millisToPosix sq.millis
                       |> Time.toDay context.here
                       |> String.fromInt)
                ]
          , occurrences sq.occurrences model.groups
          ]
    ]

numberClass : Model -> Context -> Posix -> String
numberClass model context posix =
    if CalendarUtils.toMonth model.cal.month == Time.toMonth context.here posix
    then "day-number day-number-month-current" ++
        if Time.toMonth context.here context.now ==
            Time.toMonth context.here posix
            && Time.toDay context.here context.now ==
                Time.toDay context.here posix
        then " day-number-day-current"
        else ""
    else "day-number"

occurrence : Occurrence -> Group -> Html Msg
occurrence model group =
    div [ class "occurrence"
         , style "background-color" group.background
         , style "color" group.color
         ]
        [ text model.draft.description
        ]

occurrences : List Occurrence -> List Group-> Html Msg
occurrences os gs=
    div []
        ( List.map (\o -> Group.find gs o.draft.groupId |> occurrence o ) os )
