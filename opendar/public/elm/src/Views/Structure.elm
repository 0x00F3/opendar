{- the highest-level view of the toolbar and three-pane row. -} 
module Structure exposing ( view )

import CalendarViews
import DatePane
import DateSquare exposing ( DateSquare )
import Editor
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model exposing (..)
import Msg exposing (..)
import ProfilePane
import TodosPanel exposing ( panel )
import Toolbar

datePane : Model -> Html Msg
datePane model =
    div [ class ("index-pane " ++ Model.secondPanelClass model) ]
        [ DatePane.pane model ]


editorPanel : Model ->  Html Msg
editorPanel model =
    div [ class ("index-pane focus-primary") ]
        [ Editor.view model ]


todosPanel : Model ->  Html Msg
todosPanel model =
    div [ class ("index-pane " ++ Model.secondPanelClass model) ]
        [ TodosPanel.panel model ]


view : Model -> Html Msg
view model = div[id "container"]
       [ header [attribute "role" "banner"]
            [ h1[][text "opendar"]
            ] --end header
       , div [class "content", id "content"]
           [ Toolbar.toolbar model
           , div [ class "flex-row grow-1 pane-group" ]
               [ div [ class ("index-pane grow-1 "
                                  ++ Model.firstPanelClass model )
                     ]
                     [ div [class "flex-column" ]
                           [ case model.context of
                                 Nothing ->
                                     h2[][ text "Loading..." ]
                                 Just calendar -> 
                                     CalendarViews.pane model calendar
                           ]
                     ]
               , case model.secondaryPanel of
                     DatePane ->
                         datePane model
                     SecondaryClosed ->
                         text "" --false node
                     TodosPanel ->
                         todosPanel model
               , case model.tertiaryPanel of
                     TertiaryClosed ->
                         text ""
                     Editor ->
                         editorPanel model
                     Profile ->
                         ProfilePane.pane model
               ] -- end pane-group
           ]-- end content div
       ] -- end container div
