module DatePane exposing ( pane )

import Group exposing ( Group )
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model exposing (..)
import Msg exposing (..)
import Occurrence exposing ( Occurrence ) 
import Todo exposing ( Todo )

pane : Model -> Html Msg
pane model =
    div [ class "flex-column" ]
        [ div [class "flex-row"]
              [ div [class "grow-1"]
                    [ h2 []
                          [ text model.dateSquare.local, text "\u{00A0}" ]
                    , button [ class "btn btn-default"
                             , onClick (OpenEditorForMillis model.dateSquare.millis)]
                        [text "➕"
                        ]
                    ]
                    , button [class "btn btn-default pull-right"
                             , onClick CloseSecondaryPane
                             ]
                    [ text "❌ "]
              ] 
        , occurrenceList model
        ]
        
endTime : String -> Html Msg
endTime end =
    if (end == "") then text ""
    else em [] [ text "Ends: ", text end ]

    
itemView : Occurrence -> Group -> Html Msg
itemView item group =
    div [ class "flex-row list-item-border"
        , style "color" group.color
        , style "background-color" group.background
        ]
        [ button [ class "btn-embedded"
                 , onClick (DeleteItem (Occurrence.toDeleteRequest item) )
--                 , value item.id
                ][text "❌"]
        , div [ class "grow-1 list-item" ]
              [ strong [] [ text item.draft.description ]
              , br[][]
              , span [] [ text (if (item.draft.location == "") then "" else "🗺️" ) ]
              , span [] [ text item.draft.location ]
              , br[][]
              , text ( if (item.draft.notes == "" ) then "" else "📝")
              , em [][ text item.draft.notes ]
              , br[][]
              , startTime item.start
              , br[][]
              , endTime item.end
              ]
        ]
            
occurrenceList : Model -> Html Msg
occurrenceList model =
    ul [ class "list-items" ]
        ( List.map (\o -> itemView o (Group.find model.groups o.draft.groupId) )
             model.dateSquare.occurrences)
        
         
    
startTime : String -> Html Msg
startTime start =
    if (start == "") then text ""
    else em [] [ text "Starts: ", text start ]
