{- models a list item group. The application state will have a list of them
   onhand. Each list item will definitely have exactly one. -}

module Group exposing (default, find, Group, groupDecoder, initialize )

import Json.Decode exposing ( Decoder, field, map5, int, string)

type alias Group =
    { background : String --represents hex color
    , color : String --represents hex code for font color
    , id : String
    , name : String
    , updated : Int
    }


find : List Group -> String -> Group
find range targetId =
    List.filter (\g -> g.id == targetId) range
        |> List.head
        |> \head -> case head of
                        Just group -> group
                        Nothing -> initialize


groupDecoder : Decoder Group
groupDecoder =
    map5 Group
        ( field "background" string )
        ( field "color" string )
        ( field "id" string )
        ( field "name" string )
        ( field "updated" int )

default : List Group
default =
    [ initialize ]
    
initialize : Group 
initialize =
    { background = "var(--theme-color)"
    , color = "#ffffff"
    , id = "1" --select the default group by default
    , name = "Default"
    , updated = 0 
    }

