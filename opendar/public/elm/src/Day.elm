--a model for one day displayed on the calendar
module Day exposing (..)

import Time exposing (..)

type alias Day =
    { month : Month
    , day : Int
    , weekday : Weekday
    }
