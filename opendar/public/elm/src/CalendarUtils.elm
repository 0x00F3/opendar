module CalendarUtils exposing (..)

import Time exposing ( Month (..), Posix, Weekday (..), Zone )

--type Monthable = Just Month | Just Int

--returns the numeric representation, **not the index **. This will come back
--to bite me.
monthToInt : Month -> Int
monthToInt month =
        case month of
        Jan -> 1
        Feb -> 2
        Mar -> 3
        Apr -> 4
        May -> 5
        Jun -> 6
        Jul -> 7
        Aug -> 8
        Sep -> 9
        Oct -> 10
        Nov -> 11
        Dec -> 12


posixToDateString : Zone -> Posix -> String
posixToDateString here posix =
    (Time.toYear here posix |> String.fromInt |> String.padLeft 4 '0') ++ "-"
        ++ (Time.toMonth here posix
           |> monthToInt
           |> String.fromInt
           |> String.padLeft 2 '0') ++ "-"
        ++ (Time.toDay here posix |> String.fromInt |> String.padLeft 2 '0')

toLocalMonth : Int -> String
toLocalMonth month =
    case month of
        0 -> "January"
        1 -> "February"
        2 -> "March"
        3 -> "April"
        4 -> "May"
        5 -> "June"
        6 -> "July"
        7 -> "August"
        8 -> "September"
        9 -> "October"
        10 -> "November"
        11 -> "December"
        _ -> "Month"
             

{- consider deprecating -}
toEnglishMonth : Month -> String
toEnglishMonth month =
    case month of
        Jan -> "January"
        Feb -> "February"
        Mar -> "March"
        Apr -> "April"
        May -> "May"
        Jun -> "June"
        Jul -> "July"
        Aug -> "August"
        Sep -> "September"
        Oct -> "October"
        Nov -> "November"
        Dec -> "December"

toMonth : Int -> Month
toMonth month =
    case month of
        0 -> Jan
        1 -> Feb
        2 -> Mar
        3 -> Apr
        4 -> May
        5 -> Jun
        6 -> Jul
        7 -> Aug
        8 -> Sep
        9 -> Oct
        10 -> Nov
        11 -> Dec
        _ -> Jan

    
toShortDay : Weekday -> String
toShortDay weekday =
    case weekday of
        Mon -> "M"
        Tue -> "T"
        Wed -> "W"
        Thu -> "R"
        Fri -> "F"
        Sat -> "S"
        Sun -> "U"

{- I want this funtion to be the new authoritative source for the days of the
   week order. I don't want to hard-code "Monday" as the first day a bunch
   of different places in code. -}
weekdayOrder : List Weekday
weekdayOrder =
    [ Mon, Tue, Wed, Thu, Fri, Sat, Sun ]

