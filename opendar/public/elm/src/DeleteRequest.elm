{-sub model representing the state of the editor. -}
module DeleteRequest exposing ( DeleteRequest )

--import Json.Decode exposing (Decoder, field, string)
--import Json.Decode.Pipeline exposing (hardcoded, required )
type alias DeleteRequest =
    { id : String
    , itemType : String
    , parent : String
    }         
