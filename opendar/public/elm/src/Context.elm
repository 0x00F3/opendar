--a way to model the current state of the Calendar view. 
module Context exposing (..)

import CalendarUtils exposing ( toEnglishMonth )
import CalNav
import Day exposing (..)
import Time exposing ( Month ( .. ), Posix, Weekday (..), Zone )
import TimeConvert

type alias Context =
    { here : Zone
--    , month : Month
    , now : Posix
--    , weeks : List (List Posix)
--    , year : Int
    }
{-
advanceMonth : Context -> Context
advanceMonth calendar =
    (if calendar.month == Dec then 
        calendar.year + 1
    else calendar.year
    ) |> changeMonth calendar (CalNav.advanceMonth calendar.month)
    
changeMonth : Context -> Month -> Int-> Context
changeMonth previousCalendar targetMonth targetYear =
    { previousCalendar | month = targetMonth
    , year = targetYear
--    , weeks = CalNav.displayMonth previousCalendar.here targetMonth targetYear
    }

decreaseMonth : Context -> Context
decreaseMonth calendar =
    ( if calendar.month == Dec then calendar.year - 1 else calendar.year )
        |> changeMonth calendar (CalNav.decreaseMonth calendar.month)
-}

initialize : Zone -> Posix -> Context
initialize here now =
    { here = here
--    , month = Time.toMonth here now
    , now = now
--    , weeks =
--        CalNav.displayMonth here (Time.toMonth here now) (Time.toYear here now)
--    , year = Time.toYear here now
    }
