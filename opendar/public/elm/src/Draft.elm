{-sub model representing the state of the editor. -}
module Draft exposing ( Draft, decoder, fromStartMillis, initialize, listItemCategory )

import CalendarUtils
import Json.Decode exposing (Decoder, field, string)
import Json.Decode.Pipeline exposing (hardcoded, required )
import Time exposing ( Zone )

type alias Draft =
    { description : String
    , endDate : String
    , endTime : String
    , groupColor : String
    , groupId : String --empty represents new user-created group.
    , groupName : String
    , id : String
    , location : String
    , notes : String
    , startDate : String
    , startTime : String
    , state : String --create or edit
    }

type ListItemType = Event | Todo

decoder : Decoder Draft
decoder =
    Json.Decode.succeed Draft
        |> required "description" string
        |> required "endDate" string
        |> required "endTime" string
        |> required "groupColor" string
        |> required "groupId" string
        |> required "groupName" string
        |> required "id" string
        |> required "location" string
        |> required "notes" string
        |> required "startDate" string
        |> required "startTime" string
        |> hardcoded "Edit"
         
fromStartMillis : Zone -> Int -> Draft
fromStartMillis here millis =
    { description = ""
    , endDate = ""
    , endTime = ""
    , groupColor = "#004400"
    , groupId = "1" --a specialized value representing "Default" group.
    , groupName = ""
    , id = ""
    , location = ""
    , notes = ""
    , startDate = Time.millisToPosix millis |> CalendarUtils.posixToDateString here
    , startTime = ""
    , state = "Create"
    }

initialize : Draft
initialize =
    { description = ""
    , endDate = ""
    , endTime = ""
    , groupColor = "#004400"
    , groupId = "1" --a specialized value representing "Default" group.
    , groupName = ""
    , id = ""
    , location = ""
    , notes = ""
    , startDate = ""
    , startTime = ""
    , state = "Create"
    }

listItemCategory : Draft -> String
listItemCategory draft =
    case listItemType draft of
        Todo -> "Todo"
        Event -> "Event"
                 
listItemType : Draft -> ListItemType
listItemType draft =
    if ( draft.startDate == "") then
        Todo
    else Event
