module Event exposing ( Event, eventDecoder )

import Json.Decode exposing ( Decoder, map8, field, int, string)

type alias Event = 
    { description : String
    , endMillis : Int
    , group : String --represents group id
    , id : String
    , location : String
    , notes : String
    , startMillis : Int
    , updated : Int
    }
    
eventDecoder : Decoder Event
eventDecoder =
    map8 Event
        ( field "description" string )
        ( field "endMillis" int )
        ( field "group" string )
        ( field "id" string )
        ( field "location" string )
        ( field "notes" string )
        ( field "startMillis" int )
        ( field "updated" int )
