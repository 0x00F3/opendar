port module Ports exposing (..)

import DeleteRequest exposing ( DeleteRequest )
import Draft exposing ( Draft )
import Json.Decode exposing ( Value )

port command : String -> Cmd msg

port debugLog : (String -> msg) -> Sub msg

port deleteItem : DeleteRequest -> Cmd msg
               
port deleteTodo : String -> Cmd msg
                  
port save : Draft -> Cmd msg

port setDateSquare : Int -> Cmd msg

port updateDateSquare : (Value -> msg) -> Sub msg

port updateBlockstack : (Value -> msg) -> Sub msg

port updateGroups : (Value -> msg) -> Sub msg

port updateTodos : (Value -> msg) -> Sub msg

port updateCalendar : (Value -> msg) -> Sub msg
                   
