module DateSquare exposing ( DateSquare, decoder, initialize )

import Draft exposing ( Draft )
import Json.Decode exposing ( Decoder, int, list, string )
import Json.Decode.Pipeline exposing ( required )
import Occurrence exposing ( Occurrence )

type alias DateSquare =
    { date : Int --1-31. Calculating in elm from millis causes DST problems.
    , local : String
    , millis : Int
    , occurrences : List Occurrence
    }

decoder : Decoder DateSquare
decoder =
    Json.Decode.succeed DateSquare
        |> required "date" int
        |> required "local" string
        |> required "millis" int
        |> required "occurrences" (list Occurrence.decoder)

initialize : DateSquare
initialize =
    { date = 0
    , local = ""
    , millis = 0
    , occurrences = []
    }
