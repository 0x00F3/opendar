module Todo exposing ( Todo, todoDecoder )

import Json.Decode exposing ( Decoder, field, map6, int, string)

type alias Todo =
    { description : String
    , id : String
    , group : String --represents group id
    , location : String
    , notes : String
    , updated : Int
    }

todoDecoder : Decoder Todo
todoDecoder =
    map6 Todo
        ( field "description" string )
        ( field "id" string )
        ( field "group" string )
        ( field "location" string )
        ( field "notes" string )
        ( field "updated" int )
