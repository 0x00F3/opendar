{-I want to rename this to Calendar, but it's going to be a gradual refactor
  first.-}

module Cal exposing ( Cal, decoder, initialize )

import DateSquare exposing ( DateSquare, decoder )
import Json.Decode exposing ( Decoder, int, list, string )
import Json.Decode.Pipeline exposing ( required )
import Time exposing ( Month (..) )

type alias Cal =
    { month : Int
    , year : Int
    , weeks : List ( List DateSquare )
    }

decoder : Decoder Cal
decoder =
    Json.Decode.succeed Cal
        |> required "month" int
        |> required "year" int
        |> required "weeks" ( list (list DateSquare.decoder) )

initialize : Cal
initialize =
    { month = -1
    , year = 0
    , weeks = []
    }
