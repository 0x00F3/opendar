module Blockstack exposing (Blockstack, decoder, initialize)

import Json.Decode exposing ( Decoder, bool, string )
import Json.Decode.Pipeline exposing ( required )

type alias Blockstack =
    { isSignedIn : Bool
    , username : String
    }


initialize : Blockstack
initialize =
    { isSignedIn = False
    , username = ""
    }

decoder : Decoder Blockstack
decoder =
    Json.Decode.succeed Blockstack
        |> required "isSignedIn" bool
        |> required "username" string

