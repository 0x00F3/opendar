module Occurrence exposing ( decoder, Occurrence, toDeleteRequest )

import DeleteRequest exposing ( DeleteRequest )
import Draft exposing ( Draft )
import Json.Decode exposing (Decoder, string)
import Json.Decode.Pipeline exposing (hardcoded, required)

type alias Occurrence =
    { draft : Draft
    , end : String
    , start : String
    }

decoder : Decoder Occurrence
decoder =
    Json.Decode.succeed Occurrence
        |> required "draft" Draft.decoder
        |> required "end" string
        |> required "start" string

toDeleteRequest : Occurrence -> DeleteRequest
toDeleteRequest occurrence =
    { id = occurrence.draft.id
    , parent = ""
    , itemType = "event"
    }
