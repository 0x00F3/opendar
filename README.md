# Workflows

## Local Storage
- A user does not have to be logged in to see the homepage.
- A user can log in or register to sync with Opendar storage. 

## List Items

- All list items must have a group.
- All list items may have a description.
- All list items may have a location. 
- All list items may have notes
- All list items may have reminders.

## Reminders

- A reminder must have a datetime associated with it. 
- There can be as many reminders as the user wants to add.

## TODOs

TODO list items will follow these business rules:

- TODOs will have a group. 
- TODOs may have reminders. 
- TODOs may be completed. 


# Architecture

